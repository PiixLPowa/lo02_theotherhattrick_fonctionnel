package fr.lo02.bessonpeuziat.vue;

import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import fr.lo02.bessonpeuziat.controleur.ControleurPlateau;
import fr.lo02.bessonpeuziat.modele.Joueur;
import fr.lo02.bessonpeuziat.modele.Partie;

/**
 * InterfacePlateau<br>
 * C'est la vue graphique qui gère le moteur du jeu.
 */

public class InterfacePlateau implements Observer {

	/**
	 * Fenêtre principale de l'InterfaceGameStart
	 */
	private JFrame frame;

	/**
	 * Bouton lié à la carte du milieu
	 */
	private JButton btnmid;

	/**
	 * Bouton carte 1 Joueur 1
	 */
	private JButton btnj11;

	/**
	 * Bouton carte 2 Joueur 1
	 */
	private JButton btnj12;

	/**
	 * Bouton carte 1 Joueur 2
	 */
	private JButton btnj21;

	/**
	 * Bouton carte 2 Joueur 2
	 */
	private JButton btnj22;

	/**
	 * Bouton carte 1 Joueur 3
	 */
	private JButton btnj31;

	/**
	 * Bouton carte 2 Joueur 3
	 */
	private JButton btnj32;

	/**
	 * Bouton "Oui"
	 */
	private JButton btnOui;

	/**
	 * Bouton "Non"
	 */
	private JButton btnNon;

	/**
	 * Nom du joueur 1
	 */
	private JLabel tagj1;

	/**
	 * Nom du joueur 2
	 */
	private JLabel tagj2;

	/**
	 * Nom du joueur 3
	 */
	private JLabel tagj3;

	/**
	 * Indicateur graphique pour afficher le joueur en cours
	 */
	private JLabel indicJoueur;

	/**
	 * Ligne de texte 1
	 */
	private JLabel txt;

	/**
	 * Ligne de texte 2
	 */
	private JLabel txt2;

	/**
	 * Ligne de texte 3
	 */
	private JLabel txt3;

	/**
	 * Affichage carte Pile
	 */
	private JLabel btnpile;

	/**
	 * Affichage carte Pioche
	 */
	private JLabel btnpioche;

	/**
	 * Partie associée à l'interface
	 */
	protected Partie partie;

	/**
	 * Crée une instance de InterfacePlateau
	 * 
	 * @param partie
	 *            Partie en cours
	 */
	public InterfacePlateau(Partie partie) {
		initialize();
		this.partie = partie;
		this.partie.addObserver(this);
		Iterator<Joueur> it1 = this.partie.getJoueurs().iterator();
		while (it1.hasNext()) {
			it1.next().addObserver(this);
		}
		new ControleurPlateau(this.partie, this.btnj11, this.btnj12, this.btnj21, this.btnj22, this.btnj31, this.btnj32,
				this.btnOui, this.btnNon);
	}

	/**
	 * Initialise le contenu de la fenêtre
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setSize(616, 507);
		frame.setTitle("The Other Hat Trick");
		frame.setIconImage(new ImageIcon(getClass().getResource("/icone.png")).getImage());
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.getContentPane().setLayout(null);

		this.btnOui = new JButton("Oui");
		this.btnOui.setBounds(150, 415, 117, 29);
		frame.getContentPane().add(this.btnOui);

		this.btnNon = new JButton("Non");
		this.btnNon.setBounds(350, 415, 117, 29);
		frame.getContentPane().add(this.btnNon);

		this.btnmid = new JButton(new ImageIcon(getClass().getResource("/back.png")));
		this.btnmid.setBounds(202, 188, 68, 99);
		frame.getContentPane().add(this.btnmid);

		this.btnj11 = new JButton(new ImageIcon(getClass().getResource("/back.png")));
		this.btnj11.setBounds(21, 111, 68, 99);
		frame.getContentPane().add(this.btnj11);

		this.btnj12 = new JButton(new ImageIcon(getClass().getResource("/back.png")));
		this.btnj12.setBounds(21, 228, 68, 99);
		frame.getContentPane().add(this.btnj12);

		this.btnj21 = new JButton(new ImageIcon(getClass().getResource("/back.png")));
		this.btnj21.setBounds(255, 21, 68, 99);
		frame.getContentPane().add(this.btnj21);

		this.btnj22 = new JButton(new ImageIcon(getClass().getResource("/back.png")));
		this.btnj22.setBounds(151, 21, 68, 99);
		frame.getContentPane().add(this.btnj22);

		this.btnj31 = new JButton(new ImageIcon(getClass().getResource("/back.png")));
		this.btnj31.setBounds(376, 228, 68, 99);
		frame.getContentPane().add(this.btnj31);

		this.btnj32 = new JButton(new ImageIcon(getClass().getResource("/back.png")));
		this.btnj32.setBounds(376, 111, 68, 99);
		frame.getContentPane().add(this.btnj32);

		this.btnpile = new JLabel(new ImageIcon(getClass().getResource("/backtrick.png")));
		this.btnpile.setBounds(480, 140, 97, 68);
		frame.getContentPane().add(this.btnpile);

		this.btnpioche = new JLabel(new ImageIcon(getClass().getResource("/backtrick.png")));
		this.btnpioche.setBounds(480, 228, 97, 68);
		frame.getContentPane().add(this.btnpioche);

		this.indicJoueur = new JLabel();
		this.indicJoueur.setText("•");
		this.indicJoueur.setBounds(51, 208, 10, 20);
		frame.getContentPane().add(indicJoueur);

		this.tagj1 = new JLabel();
		this.tagj1.setBounds(21, 92, 100, 16);
		frame.getContentPane().add(tagj1);

		this.tagj2 = new JLabel();
		this.tagj2.setBounds(151, 120, 100, 16);
		frame.getContentPane().add(tagj2);

		this.tagj3 = new JLabel();
		this.tagj3.setBounds(376, 92, 100, 16);
		frame.getContentPane().add(tagj3);

		this.txt = new JLabel();
		this.txt.setText("");
		this.txt.setBounds(13, 350, 824, 16);
		frame.getContentPane().add(txt);

		this.txt2 = new JLabel();
		this.txt2.setText("");
		this.txt2.setBounds(13, 370, 824, 16);
		frame.getContentPane().add(txt2);

		this.txt3 = new JLabel();
		this.txt3.setText("");
		this.txt3.setBounds(13, 390, 824, 16);
		frame.getContentPane().add(txt3);
	}

	/**
	 * Facilite le changement du label txt<br>
	 * Cette méthode s'assure que le message affiché sera la bon<br>
	 * Vient en réponse à un problème rencontré de message qui ne s'actualisait pas
	 * 
	 * @param msg
	 *            Message à afficher sur la ligne de texte
	 */
	public void setMessageTxt(String msg) {
		this.txt.setVisible(false);
		frame.getContentPane().remove(this.txt);
		this.txt.setText(msg);
		frame.getContentPane().add(txt);
		this.txt.setVisible(true);
		frame.revalidate();
	}

	/**
	 * Facilite le changement du label txt2<br>
	 * Cette méthode s'assure que le message affiché sera la bon<br>
	 * Vient en réponse à un problème rencontré de message qui ne s'actualisait pas
	 * 
	 * @param msg
	 *            Message à afficher sur la ligne de texte
	 */
	public void setMessageTxt2(String msg) {
		this.txt2.setVisible(false);
		frame.getContentPane().remove(this.txt2);
		this.txt2.setText(msg);
		frame.getContentPane().add(txt2);
		this.txt2.setVisible(true);
		frame.revalidate();
	}

	/**
	 * Facilite le changement du label txt3<br>
	 * Cette méthode s'assure que le message affiché sera la bon<br>
	 * Vient en réponse à un problème rencontré de message qui ne s'actualisait pas
	 * 
	 * @param msg
	 *            Message à afficher sur la ligne de texte
	 */
	public void setMessageTxt3(String msg) {
		this.txt3.setVisible(false);
		frame.getContentPane().remove(this.txt3);
		this.txt3.setText(msg);
		frame.getContentPane().add(txt3);
		this.txt3.setVisible(true);
		frame.revalidate();
	}

	/**
	 * Met à jour la vue en fonction de l'argument envoyé
	 * 
	 * @param o
	 *            Observable qui a envoyé la notification
	 * 
	 * @param arg
	 *            Objet envoyé par l'Observable
	 */
	@Override
	public synchronized void update(Observable o, Object arg) {
		if (o instanceof Partie) {
			if (((String) arg).equals("AffichageTour")) {
				setMessageTxt("--------------------- Tour de "
						+ this.partie.getJoueurs().get(this.partie.getJoueurEnCours()).getName() + " ("
						+ this.partie.getJoueurs().get(this.partie.getJoueurEnCours()).getScore()
						+ " points) ---------------------");
				this.tagj1.setText(this.partie.getJoueurs().get(0).getName() + " ("
						+ this.partie.getJoueurs().get(0).getScore() + ")");
				this.tagj2.setText(this.partie.getJoueurs().get(1).getName() + " ("
						+ this.partie.getJoueurs().get(1).getScore() + ")");
				this.tagj3.setText(this.partie.getJoueurs().get(2).getName() + " ("
						+ this.partie.getJoueurs().get(2).getScore() + ")");
				this.updateCartes();
				if (this.partie.getJoueurEnCours() == 0) {
					this.indicJoueur.setBounds(51, 208, 10, 20);
				}
				if (this.partie.getJoueurEnCours() == 1) {
					this.indicJoueur.setBounds(234, 62, 10, 20);
				}
				if (this.partie.getJoueurEnCours() == 2) {
					this.indicJoueur.setBounds(406, 208, 20, 20);
				}
				this.btnj11.setEnabled(false);
				this.btnj12.setEnabled(false);
				this.btnj21.setEnabled(false);
				this.btnj22.setEnabled(false);
				this.btnj31.setEnabled(false);
				this.btnj32.setEnabled(false);
				this.btnmid.setEnabled(false);
			}

			if (((String) arg).equals("AskTourSuivant")) {
				setMessageTxt3("Voulez-vous jouer le tour suivant ?");
				if (this.partie.getJoueurEnCours() == 0) {
					this.updateCarteUneJoueur2();
					this.updateCarteDeuxJoueur2();
				}
				if (this.partie.getJoueurEnCours() == 1) {
					this.updateCarteUneJoueur0();
					this.updateCarteDeuxJoueur0();
				}
				if (this.partie.getJoueurEnCours() == 2) {
					this.updateCarteUneJoueur1();
					this.updateCarteDeuxJoueur1();
				}
				this.btnOui.setEnabled(true);
				this.btnNon.setEnabled(true);
				this.btnj11.setEnabled(false);
				this.btnj12.setEnabled(false);
				this.btnj21.setEnabled(false);
				this.btnj22.setEnabled(false);
				this.btnj31.setEnabled(false);
				this.btnj32.setEnabled(false);
			}

			if (((String) arg).equals("FinPartieSuccess")) {
				setMessageTxt("The Other Hat Trick réussi ! Fin de la partie...");
				this.finPartie();
			}

			if (((String) arg).equals("FinPartieFail")) {
				setMessageTxt("The Other Hat Trick raté 3 fois ! Pénalités infligées. Fin de la partie...");
				this.finPartie();
			}

			if (((String) arg).equals("ArretVolontaire")) {
				setMessageTxt("Vous avez mis fin à la partie...");
				this.finPartie();
			}
		}

		if (o instanceof Joueur) {
			if (((String) arg).equals("TenteTrick")) {
				setMessageTxt2("Voulez-vous conserver le trick actuel ?");
				setMessageTxt3("");
				this.updateCartes();
				this.updateTrick();
			}

			if (((String) arg).equals("OrdiGardeTrick")) {
				setMessageTxt("Trick conservé (" + this.partie.getPile().getTrick().getName() + ")");
			}

			if (((String) arg).equals("OrdiChangeTrick")) {
				setMessageTxt("Un nouveau Trick a été pioché (" + this.partie.getPile().getTrick().getName() + ")");
				this.updateTrick();
			}

			if (((String) arg).contains("OrdiSuccessTrick")) {
				String string = ((String) arg);
				String[] parts = string.split("-");
				String points = parts[1];
				String msg = this.txt.getText();
				setMessageTxt(msg + " | Réussi ! | +" + points + "points");
				this.tagj1.setText(this.partie.getJoueurs().get(0).getName() + " ("
						+ this.partie.getJoueurs().get(0).getScore() + ")");
				this.tagj2.setText(this.partie.getJoueurs().get(1).getName() + " ("
						+ this.partie.getJoueurs().get(1).getScore() + ")");
				this.tagj3.setText(this.partie.getJoueurs().get(2).getName() + " ("
						+ this.partie.getJoueurs().get(2).getScore() + ")");
			}

			if (((String) arg).equals("OrdiFailedTrick")) {
				String msg = this.txt.getText();
				setMessageTxt(msg + " | Raté !");
			}

			if (((String) arg).contains("OrdiChangedProp")) {
				String string = ((String) arg);
				String[] parts = string.split("-");
				String cote1 = parts[1];
				String cote2 = parts[2];
				String namejoueur2 = parts[3];
				setMessageTxt2(
						"L'ordinateur a échangé sa carte " + cote1 + " avec la carte " + cote2 + " de " + namejoueur2);
				this.updateTrick();
			}

			if (((String) arg).equals("ChangeTrick")) {
				setMessageTxt2("Trick à jouer : " + this.partie.getPile().getTrick().toString());
				this.updateTrick();
			}

			if (((String) arg).equals("ChoixSelfCarte")) {
				setMessageTxt3("Choisissez votre carte à échanger");
				this.btnmid.setEnabled(false);
				this.btnOui.setEnabled(false);
				this.btnNon.setEnabled(false);
				if (this.partie.getJoueurEnCours() == 0) {
					this.btnj11.setEnabled(true);
					this.btnj12.setEnabled(true);
				}

				if (this.partie.getJoueurEnCours() == 1) {
					this.btnj21.setEnabled(true);
					this.btnj22.setEnabled(true);
				}

				if (this.partie.getJoueurEnCours() == 2) {
					this.btnj31.setEnabled(true);
					this.btnj32.setEnabled(true);
				}
			}

			if (((String) arg).equals("ChoixOtherCarte")) {
				setMessageTxt3("Choisissez la carte que vous voulez récupérer");
				if (this.partie.getJoueurEnCours() == 0) {
					this.btnj11.setEnabled(false);
					this.btnj12.setEnabled(false);
					this.btnj21.setEnabled(true);
					this.btnj22.setEnabled(true);
					this.btnj31.setEnabled(true);
					this.btnj32.setEnabled(true);
				}

				else if (this.partie.getJoueurEnCours() == 1) {
					this.btnj21.setEnabled(false);
					this.btnj22.setEnabled(false);
					this.btnj11.setEnabled(true);
					this.btnj12.setEnabled(true);
					this.btnj31.setEnabled(true);
					this.btnj32.setEnabled(true);
				}

				else if (this.partie.getJoueurEnCours() == 2) {
					this.btnj31.setEnabled(false);
					this.btnj32.setEnabled(false);
					this.btnj21.setEnabled(true);
					this.btnj22.setEnabled(true);
					this.btnj11.setEnabled(true);
					this.btnj12.setEnabled(true);
				}

				// IL FAUT GRISER LES CARTES QUE LE JOUEUR NE PEUT PAS RECUPERER
			}
			if (((String) arg).equals("UpdateCartes")) {
				setMessageTxt3("");
				this.updateCartes();
			}

			if (((String) arg).contains("DemanderTenterTrick")) {
				String string = ((String) arg);
				String[] parts = string.split("-");
				String carteRecup = parts[1];
				setMessageTxt2("Vous avez récupéré : " + carteRecup);
				setMessageTxt3("Voulez-vous tenter le Trick ?");
				this.btnOui.setEnabled(true);
				this.btnNon.setEnabled(true);
				this.btnj31.setEnabled(false);
				this.btnj32.setEnabled(false);
				this.btnj21.setEnabled(false);
				this.btnj22.setEnabled(false);
				this.btnj11.setEnabled(false);
				this.btnj12.setEnabled(false);
			}

			if (((String) arg).equals("SuccessTrick")) {
				setMessageTxt("--------------------- Tour de "
						+ this.partie.getJoueurs().get(this.partie.getJoueurEnCours()).getName() + " ("
						+ this.partie.getJoueurs().get(this.partie.getJoueurEnCours()).getScore()
						+ " points) ---------------------");
				setMessageTxt2("");
				setMessageTxt3("Trick réussi ! Choisissez quelle carte mettre au milieu");
				this.btnNon.setEnabled(false);
				this.btnOui.setEnabled(false);
				this.tagj1.setText(this.partie.getJoueurs().get(0).getName() + " ("
						+ this.partie.getJoueurs().get(0).getScore() + ")");
				this.tagj2.setText(this.partie.getJoueurs().get(1).getName() + " ("
						+ this.partie.getJoueurs().get(1).getScore() + ")");
				this.tagj3.setText(this.partie.getJoueurs().get(2).getName() + " ("
						+ this.partie.getJoueurs().get(2).getScore() + ")");
				if (this.partie.getJoueurEnCours() == 0) {
					this.updateCarteUneJoueur0();
					this.updateCarteDeuxJoueur0();
					this.btnj11.setEnabled(true);
					this.btnj12.setEnabled(true);
					this.btnj21.setEnabled(false);
					this.btnj22.setEnabled(false);
					this.btnj31.setEnabled(false);
					this.btnj32.setEnabled(false);
				}
				if (this.partie.getJoueurEnCours() == 1) {
					this.updateCarteUneJoueur1();
					this.updateCarteDeuxJoueur1();
					this.btnj11.setEnabled(false);
					this.btnj12.setEnabled(false);
					this.btnj21.setEnabled(true);
					this.btnj22.setEnabled(true);
					this.btnj31.setEnabled(false);
					this.btnj32.setEnabled(false);
				}
				if (this.partie.getJoueurEnCours() == 2) {
					this.updateCarteUneJoueur2();
					this.updateCarteDeuxJoueur2();
					this.btnj11.setEnabled(false);
					this.btnj12.setEnabled(false);
					this.btnj21.setEnabled(false);
					this.btnj22.setEnabled(false);
					this.btnj31.setEnabled(true);
					this.btnj32.setEnabled(true);
				}
			}

			if (((String) arg).equals("FailTrick")) {
				setMessageTxt2("");
				setMessageTxt3("Trick raté. Choisissez une carte à retourner");
				this.btnNon.setEnabled(false);
				this.btnOui.setEnabled(false);
				if (this.partie.getJoueurEnCours() == 0) {
					this.btnj11.setIcon(new ImageIcon(getClass().getResource("/back.png")));
					this.btnj12.setIcon(new ImageIcon(getClass().getResource("/back.png")));
					this.btnj11.setEnabled(true);
					this.btnj12.setEnabled(true);
					this.btnj21.setEnabled(false);
					this.btnj22.setEnabled(false);
					this.btnj31.setEnabled(false);
					this.btnj32.setEnabled(false);
				}

				if (this.partie.getJoueurEnCours() == 1) {
					this.btnj21.setIcon(new ImageIcon(getClass().getResource("/back.png")));
					this.btnj22.setIcon(new ImageIcon(getClass().getResource("/back.png")));
					this.btnj21.setEnabled(true);
					this.btnj22.setEnabled(true);
					this.btnj11.setEnabled(false);
					this.btnj12.setEnabled(false);
					this.btnj31.setEnabled(false);
					this.btnj32.setEnabled(false);
				}

				if (this.partie.getJoueurEnCours() == 2) {
					this.btnj31.setIcon(new ImageIcon(getClass().getResource("/back.png")));
					this.btnj32.setIcon(new ImageIcon(getClass().getResource("/back.png")));
					this.btnj31.setEnabled(true);
					this.btnj32.setEnabled(true);
					this.btnj21.setEnabled(false);
					this.btnj22.setEnabled(false);
					this.btnj11.setEnabled(false);
					this.btnj12.setEnabled(false);
				}
			}
		}
	}

	/**
	 * Met à jour l'affichage de toutes les PropCard
	 */
	private void updateCartes() {
		this.updateCarteUneJoueur0();
		this.updateCarteDeuxJoueur0();
		this.updateCarteUneJoueur1();
		this.updateCarteDeuxJoueur1();
		this.updateCarteUneJoueur2();
		this.updateCarteDeuxJoueur2();
	}

	/**
	 * Met à jour l'affichage du Trick de la Pile
	 */
	private void updateTrick() {
		if (this.partie.getPile().getTrick().getName() == "The Other Hat Trick") {
			this.btnpile.setIcon(new ImageIcon(getClass().getResource("/theotherhattrick1.png")));
		}

		if (this.partie.getPile().getTrick().getName() == "The Bunch of Carrots") {
			this.btnpile.setIcon(new ImageIcon(getClass().getResource("/bunchofcarrots1.png")));
		}

		if (this.partie.getPile().getTrick().getName() == "The Hungry Rabbit") {
			this.btnpile.setIcon(new ImageIcon(getClass().getResource("/hungryrabbit1.png")));
		}

		if (this.partie.getPile().getTrick().getName() == "The Vegetable Patch") {
			this.btnpile.setIcon(new ImageIcon(getClass().getResource("/vegetablepatch1.png")));
		}

		if (this.partie.getPile().getTrick().getName() == "The Rabbit That Didn't Like Carrots") {
			this.btnpile.setIcon(new ImageIcon(getClass().getResource("/rabbitdidntlikecarrot1.png")));
		}

		if (this.partie.getPile().getTrick().getName() == "The Pair Of Rabbits") {
			this.btnpile.setIcon(new ImageIcon(getClass().getResource("/pairofrabbit1.png")));
		}

		if (this.partie.getPile().getTrick().getName() == "The Vegetable Hat Trick") {
			this.btnpile.setIcon(new ImageIcon(getClass().getResource("/vegetablehattrick1.png")));
		}

		if (this.partie.getPile().getTrick().getName() == "The Carrot Hat Trick") {
			this.btnpile.setIcon(new ImageIcon(getClass().getResource("/carrothattrick1.png")));
		}

		if (this.partie.getPile().getTrick().getName() == "The Slightly Easier Hat Trick") {
			this.btnpile.setIcon(new ImageIcon(getClass().getResource("/slightyeasierhattrick1.png")));
		}

		if (this.partie.getPile().getTrick().getName() == "The Hat Trick") {
			this.btnpile.setIcon(new ImageIcon(getClass().getResource("/hattrick1.png")));
		}

		if (this.partie.getPile().getTrick().getName() == "The Dead Of Rabbit") {
			this.btnpile.setIcon(new ImageIcon(getClass().getResource("/thedeadofrabbit1.png")));
		}

		if (this.partie.getPile().getTrick().getName() == "The Thug Rabbit") {
			this.btnpile.setIcon(new ImageIcon(getClass().getResource("/thugrabbit1.png")));
		}

		if (this.partie.getPile().getTrick().getName() == "The Head Gear") {
			this.btnpile.setIcon(new ImageIcon(getClass().getResource("/theheadgear1.png")));
		}
	}

	/**
	 * Met à jour l'affichage de la carte Gauche du joueur 1
	 */
	private void updateCarteUneJoueur0() {
		if (this.partie.getJoueurs().get(0).getCarteUne().isVisible() || this.partie.getJoueurEnCours() == 0) {
			if (this.partie.getJoueurs().get(0).getCarteUne().getProp() == "The Lettuce") {
				this.btnj11.setIcon(new ImageIcon(getClass().getResource("/lettuce1.png")));
			}

			if (this.partie.getJoueurs().get(0).getCarteUne().getProp() == "Carrots") {
				this.btnj11.setIcon(new ImageIcon(getClass().getResource("/carrot1.png")));
			}

			if (this.partie.getJoueurs().get(0).getCarteUne().getProp() == "The Hat") {
				this.btnj11.setIcon(new ImageIcon(getClass().getResource("/hat1.png")));
			}

			if (this.partie.getJoueurs().get(0).getCarteUne().getProp() == "The Rabbit") {
				this.btnj11.setIcon(new ImageIcon(getClass().getResource("/rabbit1.png")));
			}

			if (this.partie.getJoueurs().get(0).getCarteUne().getProp() == "The Other Rabbit") {
				this.btnj11.setIcon(new ImageIcon(getClass().getResource("/otherrabbit1.png")));
			}

			if (this.partie.getJoueurs().get(0).getCarteUne().getProp() == "The Carnivorous Rabbit") {
				this.btnj11.setIcon(new ImageIcon(getClass().getResource("/carnivorous1.png")));
			}

			if (this.partie.getJoueurs().get(0).getCarteUne().getProp() == "The Cap") {
				this.btnj11.setIcon(new ImageIcon(getClass().getResource("/cap1.png")));
			}
		} else {
			this.btnj11.setIcon(new ImageIcon(getClass().getResource("/back.png")));
		}
	}

	/**
	 * Met à jour l'affichage de la carte Droite du joueur 1
	 */
	private void updateCarteDeuxJoueur0() {
		if (this.partie.getJoueurs().get(0).getCarteDeux().isVisible() || this.partie.getJoueurEnCours() == 0) {
			if (this.partie.getJoueurs().get(0).getCarteDeux().getProp() == "The Lettuce") {
				this.btnj12.setIcon(new ImageIcon(getClass().getResource("/lettuce1.png")));
			}

			if (this.partie.getJoueurs().get(0).getCarteDeux().getProp() == "Carrots") {
				this.btnj12.setIcon(new ImageIcon(getClass().getResource("/carrot1.png")));
			}

			if (this.partie.getJoueurs().get(0).getCarteDeux().getProp() == "The Hat") {
				this.btnj12.setIcon(new ImageIcon(getClass().getResource("/hat1.png")));
			}

			if (this.partie.getJoueurs().get(0).getCarteDeux().getProp() == "The Rabbit") {
				this.btnj12.setIcon(new ImageIcon(getClass().getResource("/rabbit1.png")));
			}

			if (this.partie.getJoueurs().get(0).getCarteDeux().getProp() == "The Other Rabbit") {
				this.btnj12.setIcon(new ImageIcon(getClass().getResource("/otherrabbit1.png")));
			}

			if (this.partie.getJoueurs().get(0).getCarteDeux().getProp() == "The Carnivorous Rabbit") {
				this.btnj12.setIcon(new ImageIcon(getClass().getResource("/carnivorous1.png")));
			}

			if (this.partie.getJoueurs().get(0).getCarteDeux().getProp() == "The Cap") {
				this.btnj12.setIcon(new ImageIcon(getClass().getResource("/cap1.png")));
			}
		} else {
			this.btnj12.setIcon(new ImageIcon(getClass().getResource("/back.png")));
		}
	}

	/**
	 * Met à jour l'affichage de la carte Gauche du joueur 2
	 */
	private void updateCarteUneJoueur1() {
		if (this.partie.getJoueurs().get(1).getCarteUne().isVisible() || this.partie.getJoueurEnCours() == 1) {
			if (this.partie.getJoueurs().get(1).getCarteUne().getProp() == "The Lettuce") {
				this.btnj21.setIcon(new ImageIcon(getClass().getResource("/lettuce1.png")));
			}

			if (this.partie.getJoueurs().get(1).getCarteUne().getProp() == "Carrots") {
				this.btnj21.setIcon(new ImageIcon(getClass().getResource("/carrot1.png")));
			}

			if (this.partie.getJoueurs().get(1).getCarteUne().getProp() == "The Hat") {
				this.btnj21.setIcon(new ImageIcon(getClass().getResource("/hat1.png")));
			}

			if (this.partie.getJoueurs().get(1).getCarteUne().getProp() == "The Rabbit") {
				this.btnj21.setIcon(new ImageIcon(getClass().getResource("/rabbit1.png")));
			}

			if (this.partie.getJoueurs().get(1).getCarteUne().getProp() == "The Other Rabbit") {
				this.btnj21.setIcon(new ImageIcon(getClass().getResource("/otherrabbit1.png")));
			}

			if (this.partie.getJoueurs().get(1).getCarteUne().getProp() == "The Carnivorous Rabbit") {
				this.btnj21.setIcon(new ImageIcon(getClass().getResource("/carnivorous1.png")));
			}

			if (this.partie.getJoueurs().get(1).getCarteUne().getProp() == "The Cap") {
				this.btnj21.setIcon(new ImageIcon(getClass().getResource("/cap1.png")));
			}
		} else {
			this.btnj21.setIcon(new ImageIcon(getClass().getResource("/back.png")));
		}
	}

	/**
	 * Met à jour l'affichage de la carte Droite du joueur 2
	 */
	private void updateCarteDeuxJoueur1() {
		if (this.partie.getJoueurs().get(1).getCarteDeux().isVisible() || this.partie.getJoueurEnCours() == 1) {
			if (this.partie.getJoueurs().get(1).getCarteDeux().getProp() == "The Lettuce") {
				this.btnj22.setIcon(new ImageIcon(getClass().getResource("/lettuce1.png")));
			}

			if (this.partie.getJoueurs().get(1).getCarteDeux().getProp() == "Carrots") {
				this.btnj22.setIcon(new ImageIcon(getClass().getResource("/carrot1.png")));
			}

			if (this.partie.getJoueurs().get(1).getCarteDeux().getProp() == "The Hat") {
				this.btnj22.setIcon(new ImageIcon(getClass().getResource("/hat1.png")));
			}

			if (this.partie.getJoueurs().get(1).getCarteDeux().getProp() == "The Rabbit") {
				this.btnj22.setIcon(new ImageIcon(getClass().getResource("/rabbit1.png")));
			}

			if (this.partie.getJoueurs().get(1).getCarteDeux().getProp() == "The Other Rabbit") {
				this.btnj22.setIcon(new ImageIcon(getClass().getResource("/otherrabbit1.png")));
			}

			if (this.partie.getJoueurs().get(1).getCarteDeux().getProp() == "The Carnivorous Rabbit") {
				this.btnj22.setIcon(new ImageIcon(getClass().getResource("/carnivorous1.png")));
			}

			if (this.partie.getJoueurs().get(1).getCarteDeux().getProp() == "The Cap") {
				this.btnj22.setIcon(new ImageIcon(getClass().getResource("/cap1.png")));
			}
		} else {
			this.btnj22.setIcon(new ImageIcon(getClass().getResource("/back.png")));
		}
	}

	/**
	 * Met à jour l'affichage de la carte Gauche du joueur 3
	 */
	private void updateCarteUneJoueur2() {
		if (this.partie.getJoueurs().get(2).getCarteUne().isVisible() || this.partie.getJoueurEnCours() == 2) {
			if (this.partie.getJoueurs().get(2).getCarteUne().getProp() == "The Lettuce") {
				this.btnj31.setIcon(new ImageIcon(getClass().getResource("/lettuce1.png")));
			}

			if (this.partie.getJoueurs().get(2).getCarteUne().getProp() == "Carrots") {
				this.btnj31.setIcon(new ImageIcon(getClass().getResource("/carrot1.png")));
			}

			if (this.partie.getJoueurs().get(2).getCarteUne().getProp() == "The Hat") {
				this.btnj31.setIcon(new ImageIcon(getClass().getResource("/hat1.png")));
			}

			if (this.partie.getJoueurs().get(2).getCarteUne().getProp() == "The Rabbit") {
				this.btnj31.setIcon(new ImageIcon(getClass().getResource("/rabbit1.png")));
			}

			if (this.partie.getJoueurs().get(2).getCarteUne().getProp() == "The Other Rabbit") {
				this.btnj31.setIcon(new ImageIcon(getClass().getResource("/otherrabbit1.png")));
			}

			if (this.partie.getJoueurs().get(2).getCarteUne().getProp() == "The Carnivorous Rabbit") {
				this.btnj31.setIcon(new ImageIcon(getClass().getResource("/carnivorous1.png")));
			}

			if (this.partie.getJoueurs().get(2).getCarteUne().getProp() == "The Cap") {
				this.btnj31.setIcon(new ImageIcon(getClass().getResource("/cap1.png")));
			}
		} else {
			this.btnj31.setIcon(new ImageIcon(getClass().getResource("/back.png")));
		}
	}

	/**
	 * Met à jour l'affichage de la carte Droite du joueur 3
	 */
	private void updateCarteDeuxJoueur2() {
		if (this.partie.getJoueurs().get(2).getCarteDeux().isVisible() || this.partie.getJoueurEnCours() == 2) {
			if (this.partie.getJoueurs().get(2).getCarteDeux().getProp() == "The Lettuce") {
				this.btnj32.setIcon(new ImageIcon(getClass().getResource("/lettuce1.png")));
			}

			if (this.partie.getJoueurs().get(2).getCarteDeux().getProp() == "Carrots") {
				this.btnj32.setIcon(new ImageIcon(getClass().getResource("/carrot1.png")));
			}

			if (this.partie.getJoueurs().get(2).getCarteDeux().getProp() == "The Hat") {
				this.btnj32.setIcon(new ImageIcon(getClass().getResource("/hat1.png")));
			}

			if (this.partie.getJoueurs().get(2).getCarteDeux().getProp() == "The Rabbit") {
				this.btnj32.setIcon(new ImageIcon(getClass().getResource("/rabbit1.png")));
			}

			if (this.partie.getJoueurs().get(2).getCarteDeux().getProp() == "The Other Rabbit") {
				this.btnj32.setIcon(new ImageIcon(getClass().getResource("/otherrabbit1.png")));
			}

			if (this.partie.getJoueurs().get(2).getCarteDeux().getProp() == "The Carnivorous Rabbit") {
				this.btnj32.setIcon(new ImageIcon(getClass().getResource("/carnivorous1.png")));
			}

			if (this.partie.getJoueurs().get(2).getCarteDeux().getProp() == "The Cap") {
				this.btnj32.setIcon(new ImageIcon(getClass().getResource("/cap1.png")));
			}
		} else {
			this.btnj32.setIcon(new ImageIcon(getClass().getResource("/back.png")));
		}
	}

	/**
	 * Modifie l'affichage de la fenêtre pour l'adapter à la fin de la partie<br>
	 * Affiche les scores, le bouton pour fermer la fenêtre...
	 */
	private void finPartie() {
		setMessageTxt2("Scores des joueurs : " + this.partie.getJoueurs().get(0).getName() + " : "
				+ this.partie.getJoueurs().get(0).getScore() + " | " + this.partie.getJoueurs().get(1).getName() + " : "
				+ this.partie.getJoueurs().get(1).getScore() + " | " + this.partie.getJoueurs().get(2).getName() + " : "
				+ this.partie.getJoueurs().get(2).getScore() + " | | " + this.partie.getVainqueur().getName()
				+ " remporte la partie !");
		this.tagj1.setText(
				this.partie.getJoueurs().get(0).getName() + " (" + this.partie.getJoueurs().get(0).getScore() + ")");
		this.tagj2.setText(
				this.partie.getJoueurs().get(1).getName() + " (" + this.partie.getJoueurs().get(1).getScore() + ")");
		this.tagj3.setText(
				this.partie.getJoueurs().get(2).getName() + " (" + this.partie.getJoueurs().get(2).getScore() + ")");
		setMessageTxt3("Appuyez sur \"Terminer\" ou fermez la fenêtre...");
		this.btnOui.setEnabled(true);
		this.btnOui.setText("Terminer");
		this.btnOui.setBounds(250, 415, 117, 29);
		this.btnNon.setVisible(false);
		this.btnj11.setEnabled(false);
		this.btnj12.setEnabled(false);
		this.btnj21.setEnabled(false);
		this.btnj22.setEnabled(false);
		this.btnj31.setEnabled(false);
		this.btnj32.setEnabled(false);
	}
}

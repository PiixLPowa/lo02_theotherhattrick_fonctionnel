package fr.lo02.bessonpeuziat.modele;

import java.util.Collections;
import java.util.Iterator;

/**
 * Extension<br>
 * Classe qui distribue les cartes en fonction du choix de les intégrer ou non.
 */

public class Extension {

	/**
	 * Constructeur d'Extension
	 */
	public Extension() {
	}

	/**
	 * Méthode pour créer les cartes standard, les mélanger, et les mettre dans la
	 * Pile et la Pioche de la Partie.<br>
	 * Les PropCards sont stockées dans le tableau tabPropCards de la partie et
	 * seront distribuées plus tard.
	 * 
	 * @param p
	 *            La Partie dans laquelle on crée les cartes
	 */
	public void creerCartes(Partie p) {
		// On ajoute toutes les PropCard à la LinkedList tabPropCards
		p.tabPropCards.add(new PropCard(enumPropCard.TheLettuce));
		p.tabPropCards.add(new PropCard(enumPropCard.TheHat));
		p.tabPropCards.add(new PropCard(enumPropCard.TheRabbit));
		p.tabPropCards.add(new PropCard(enumPropCard.TheOtherRabbit));
		p.tabPropCards.add(new PropCard(enumPropCard.Carrots));
		p.tabPropCards.add(new PropCard(enumPropCard.Carrots));
		p.tabPropCards.add(new PropCard(enumPropCard.Carrots));
		Collections.shuffle(p.tabPropCards);

		// On boucle sur les différents Tricks déclarés dans le enum pour les ajouter à
		// l'ArrayList
		for (enumTrick trick : enumTrick.values()) {
			if (trick.getName() != "The Other Hat Trick" && trick.getName() != "The Dead Of Rabbit"
					&& trick.getName() != "The Thug Rabbit" && trick.getName() != "The Head Gear") {
				p.tabTrickCards.add(new Trick(trick));
			}
		}
		// On mélange ces cartes, puis on ajoute TheOtherHatTrick au début (bas de la
		// pile)
		Collections.shuffle(p.tabTrickCards);
		p.tabTrickCards.add(0, new Trick(enumTrick.TheOtherHatTrick));

		// Ajout des Tricks à la Pioche
		Iterator<Trick> itTricks = p.tabTrickCards.iterator();
		while (itTricks.hasNext()) {
			p.pioche.addTrickToPioche(itTricks.next());
		}
	}

	/**
	 * Méthode pour créer les cartes d'extension, les mélanger, et les mettre dans
	 * la Pile et la Pioche de la Partie.<br>
	 * Les PropCards sont stockées dans le tableau tabPropCards de la partie et
	 * seront distribuées plus tard.
	 * 
	 * @param p
	 *            La Partie dans laquelle on crée les cartes
	 */
	public void creerCartesExt(Partie p) {
		// On ajoute toutes les PropCard à la LinkedList tabPropCards
		p.tabPropCards.add(new PropCard(enumPropCard.TheLettuce));
		p.tabPropCards.add(new PropCard(enumPropCard.TheHat));
		p.tabPropCards.add(new PropCard(enumPropCard.TheRabbit));
		p.tabPropCards.add(new PropCard(enumPropCard.TheOtherRabbit));
		p.tabPropCards.add(new PropCard(enumPropCard.Carrots));
		p.tabPropCards.add(new PropCard(enumPropCard.TheCarnivorousRabbit));
		p.tabPropCards.add(new PropCard(enumPropCard.TheCap));
		Collections.shuffle(p.tabPropCards);

		// On boucle sur les différents Tricks déclarés dans le enum pour les ajouter à
		// l'ArrayList
		for (enumTrick trick : enumTrick.values()) {
			if (trick.getName() != "The Other Hat Trick" && trick.getName() != "The Bunch of Carrots") {
				p.tabTrickCards.add(new Trick(trick));
			}
		}
		// On mélange ces cartes, puis on ajoute TheOtherHatTrick au début (bas de la
		// pile)
		Collections.shuffle(p.tabTrickCards);
		p.tabTrickCards.add(0, new Trick(enumTrick.TheOtherHatTrick));

		// Ajout des Tricks à la Pioche
		Iterator<Trick> itTricks = p.tabTrickCards.iterator();
		while (itTricks.hasNext()) {
			p.pioche.addTrickToPioche(itTricks.next());
		}
	}
}

package fr.lo02.bessonpeuziat.modele;

/**
 * Représente un Trick
 */

public class Trick extends Carte {

	/**
	 * Nom du Trick
	 */
	private String name;

	/**
	 * Nombre de points que rapporte le Trick
	 */
	private Integer valeur;

	/**
	 * Tableau de PropCard qui représente les PropCard du côté Gauche du Trick
	 */
	private PropCard[] prop1;

	/**
	 * Tableau de PropCard qui représente les PropCard du côté Droite du Trick
	 */
	private PropCard[] prop2;

	/**
	 * Crée une instance de Trick
	 * 
	 * @param trick
	 *            Le nom du Trick que l'on veut créer
	 */
	public Trick(enumTrick trick) {
		this.name = trick.getName();
		this.valeur = trick.getValeur();
		this.prop1 = trick.getPropCardLeft();
		this.prop2 = trick.getPropCardRight();
	}

	/**
	 * @return Le tableau de PropCard du côté Gauche du Trick
	 */
	public PropCard[] getPropCardLeft() {
		return this.prop1;
	}

	/**
	 * @return Le tableau de PropCard du côté Droite du Trick
	 */
	public PropCard[] getPropCardRight() {
		return this.prop2;
	}

	/**
	 * @return Le nom du Trick
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @return Le nombre de points que rapporte le Trick
	 */
	public Integer getValue() {
		return this.valeur;
	}

	/**
	 * @return Affiche proprement un Trick sous forme de String
	 */
	public String toString() {
		if (this.prop1[0].getProp() == this.prop1[1].getProp()) {
			if (this.prop2[0].getProp() == this.prop2[1].getProp()) {
				return this.name + " est composé de " + this.prop1[0].getProp() + " et de " + this.prop2[0].getProp()
						+ " (" + this.valeur + " points)";
			} else {
				return this.name + " est composé de " + this.prop1[0].getProp() + " et de " + this.prop2[0].getProp()
						+ " ou " + this.prop2[1].getProp() + " (" + this.valeur + " points)";
			}
		} else if (this.prop2[0].getProp() == this.prop2[1].getProp()) {
			return this.name + " est composé de " + this.prop1[0].getProp() + " ou " + this.prop1[1].getProp()
					+ " et de " + this.prop2[0].getProp() + " (" + this.valeur + " points)";
		} else {
			return this.name + " est composé de " + this.prop1[0].getProp() + " ou " + this.prop1[1].getProp()
					+ " et de " + this.prop2[0].getProp() + " ou " + this.prop2[1].getProp() + " (" + this.valeur
					+ " points)";
		}
	}
}

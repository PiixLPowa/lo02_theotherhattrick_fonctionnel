package fr.lo02.bessonpeuziat.modele;

/**
 * Représente une PropCard
 */

public class PropCard extends Carte {

	/**
	 * Nom de la PropCard
	 */
	private enumPropCard name;

	/**
	 * Constructeur de la PropCard
	 * 
	 * @param name
	 *            Le nom de la PropCard (Sans espaces)
	 */
	public PropCard(enumPropCard name) {
		this.name = name;
	}

	/**
	 * Renvoie le nom de la PropCard sous forme de chaîne de caractère
	 * 
	 * @return Le nom de la PropCard
	 */
	public String getProp() {
		return this.name.getName();
	}
}

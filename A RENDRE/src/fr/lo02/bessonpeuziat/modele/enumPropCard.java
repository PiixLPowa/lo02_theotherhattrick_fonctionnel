package fr.lo02.bessonpeuziat.modele;

/**
 * Enumération de PropCards<br>
 * Liste les différentes PropCard disponibles
 */

public enum enumPropCard {

	/**
	 * PropCard Lettuce
	 */
	TheLettuce("The Lettuce"),

	/**
	 * PropCard Carrots
	 */
	Carrots("Carrots"),

	/**
	 * PropCard The Hat
	 */
	TheHat("The Hat"),

	/**
	 * PropCard The Rabbit
	 */
	TheRabbit("The Rabbit"),

	/**
	 * PropCard The Other Rabbit
	 */
	TheOtherRabbit("The Other Rabbit"),

	/**
	 * PropCard The Carnivorous Rabbit
	 */
	TheCarnivorousRabbit("The Carnivorous Rabbit"),

	/**
	 * PropCard The Cap
	 */
	TheCap("The Cap");

	/**
	 * Nom de la PropCard
	 */
	private String name = "";

	/**
	 * Constructeur de la enumPropCard
	 * 
	 * @param name
	 *            Le nom de la PropCard
	 */
	private enumPropCard(String name) {
		this.name = name;
	}

	/**
	 * Renvoie le nom d'une enumPropCard
	 * 
	 * @return Le nom de la PropCard
	 */
	public String getName() {
		return this.name;
	}

}

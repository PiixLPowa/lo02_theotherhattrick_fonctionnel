package fr.lo02.bessonpeuziat.modele;

import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;

import fr.lo02.bessonpeuziat.controleur.ControleurOuiNon;
import fr.lo02.bessonpeuziat.controleur.ControleurPlateau;
import fr.lo02.bessonpeuziat.vue.InterfaceGameStart;
import fr.lo02.bessonpeuziat.vue.InterfacePlateau;

/**
 * Représente une Partie<br>
 * C'est la classe principale.
 */

public class Partie extends Observable implements Observer {

	/**
	 * Compteur de tour
	 */
	private Integer tour = 0;

	/**
	 * Nombre de joueurs réels dans la partie
	 */
	private Integer nombreJoueursReels = 0;

	/**
	 * Nombre de joueurs virtuels dans la partie
	 */
	private Integer nombreJoueursVirtuels = 0;

	/**
	 * Vainqueur de la partie
	 */
	private Joueur vainqueur;

	/**
	 * Extension utilisée
	 */
	private Extension extension;

	/**
	 * Défini si la partie est terminée ou non
	 */
	protected Boolean finPartie = false;

	/**
	 * Joueur en cours (va de 0 à 2) pour parcourir le tableau des joueurs
	 */
	protected Integer joueurEnCours = 0;

	/**
	 * Nombre de tentatives effectuées pour le Trick final
	 */
	protected Integer theOtherHatTrickCount = 0;

	/**
	 * Pile de Tricks
	 */
	protected Pile pile = new Pile();

	/**
	 * Pioche de Tricks
	 */
	protected Pioche pioche = new Pioche();

	/**
	 * Réponse récupérée par le Scanner ou le Controleur
	 */
	protected String reponse = "";

	/**
	 * PropCard du milieu
	 */
	protected PropCard carteMilieu;

	/**
	 * Tableau contenant les joueurs de la Partie
	 */
	protected ArrayList<Joueur> tabJoueurs = new ArrayList<Joueur>();

	/**
	 * Tableau contenant les Tricks pour l'initialisation de la Partie
	 */
	protected ArrayList<Trick> tabTrickCards = new ArrayList<Trick>();

	/**
	 * Tableau contenant les PropCards pour l'initialisation de la Partie
	 */
	protected LinkedList<PropCard> tabPropCards = new LinkedList<PropCard>();

	/**
	 * Défini si la Partie utilise la variante 1 ou non
	 */
	private boolean variante1 = false;

	/**
	 * Défini si la Partie utilise la variante 2 ou non
	 */
	private boolean variante2 = false;

	/**
	 * Constructeur de Partie
	 */
	public Partie() {
		Partie p = this;
		// Création de l'interface graphique
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new InterfaceGameStart(p);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		ConsoleInput console = new ConsoleInput(p);
		console.demarrer();

		// Message de bienvenue
		reponse = "";
		while (!(reponse.equalsIgnoreCase("Oui")) && !(reponse.equalsIgnoreCase("Non"))) {
			System.out.println("Voulez-vous consulter les règles standards du jeu ?");
			recupererInput();
		}

		if (reponse.equalsIgnoreCase("Oui")) {
			System.out.println(
					"The Other Hat Trick est un jeu de carte. \nL'objectif est de former des tricks à l'aide de cartes dites Props.\nChaque joueur se voit distribuer deux props.\nA son tour, celui-ci doit choisir entre un trick face visible et un trick face cachée à réaliser. \nUne fois le trick choisi, le joueur DOIT OBLIGATOIREMENT échanger un de ses props avec un prop d'un autre joueur face cachée ou face visible devant eux.\nCela fait, le joueur tente alors le trick, si celui-ci est concluant il remporte le nombre de points indiqués. \nIl devra alors échanger un de ses props avec la carte du milieu, puis reposer se deux props face cachée devant lui. \nCependant si le trick n'est pas réalisé, il devra alors retourner face visible un de ses props devant lui. \nLors du dernier tour de jeu, un trick spécial apparait The Other Hat Trick. \nSon fonctionnement est un peu différent des autre, car si quelqu'un arrive à le réaliser, il gagne 6 points. \nNéanmoins, si personne ne le réussit, les joueurs possédant les cartes The Hat et The Other Rabbit ont une pénalité de 3 points. \nLe vainqueur est le joueur ayant accumulé le plus de points après ce tour.");
			this.sendMessage("DemandeRègles");
		}
		if (reponse.equalsIgnoreCase("Non")) {
			System.out.println("Très bien, continuons");
		}
		this.sendMessage("AfficheQ2");

		// Choix de la variante
		reponse = "";
		while (!(reponse.equalsIgnoreCase("Oui")) && !(reponse.equalsIgnoreCase("Non"))) {
			System.out.println("*****Voulez-vous jouer avec les règles standard ? Tapez Oui ou Non *****");
			recupererInput();
		}

		if (reponse.equalsIgnoreCase("Oui")) {
			System.out.println(">>> DEMARRAGE D'UNE NOUVELLE PARTIE AVEC LES REGLES STANDARD <<<");
			this.sendMessage("AfficheQExtension");
		}

		else {
			this.sendMessage("AfficheChoixVariante");
			reponse = "";
			while (!(reponse.equals("1")) && !(reponse.equals("2"))) {
				System.out.println("Quelle variante voulez-vous utiliser ? Tapez 1 ou 2");
				recupererInput();
			}
			if (reponse.equals("1")) {
				System.out.println(">>> DEMARRAGE D'UNE NOUVELLE PARTIE AVEC LES REGLES DE VARIANTE 1 <<<");
				variante1 = true;
				System.out.println(
						"Dans cette variante, il vous faut jouer encore plus stratégique :\n A chaque trick réussi, vous cumulez un malus s'appliquant au nombre de points que vous gagnerez sur le prochain trick réussi !");
			} else {
				System.out.println(">>> DEMARRAGE D'UNE NOUVELLE PARTIE AVEC LES REGLES DE VARIANTE 2 <<<");
				variante2 = true;
				System.out.println(
						"Ici, chaque changement de joueur augmente le nombre de points \n qu'un trick rapporte ! Gare aux fins de parties !!");
			}
			this.sendMessage("AfficheQExtension");
		}
		extension = new Extension();

		// Choix du jeu avec ou sans extensions
		reponse = "";
		while (!(reponse.equalsIgnoreCase("Oui")) && !(reponse.equalsIgnoreCase("Non"))) {
			System.out.println("Souhaitez-vous intégrer les cartes d'extension ? Tapez Oui ou Non.");
			recupererInput();
		}

		if (reponse.equalsIgnoreCase("Oui")) {
			extension.creerCartesExt(this);
			System.out.println(
					"Deux nouveaux props apparaissent : The Carnivorous Rabbit et The Cap à la place de 2 Carrots. \nTrois nouveaux tricks sont réalisables : The Dead Of Rabbit, \nThe Thug Rabbit et The Head Gear. Néanmoins, plus assez de carottes donc plus de The Bunch of Carrots !");
		}

		else {
			extension.creerCartes(this);
		}
		this.sendMessage("AfficheChoixJoueurs");

		// Configuration des joueurs
		for (Integer i = 1; i <= 3; i++) {
			reponse = "";
			while (reponse == "") {
				System.out.println(
						"Saisissez le nom du joueur " + i + " (Tapez \"Virtuel\" pour ajouter un joueur virtuel) :");
				recupererInput();
			}
			if (reponse.equalsIgnoreCase("Virtuel")) {
				nombreJoueursVirtuels++;
				tabJoueurs.add(new Virtuel("Virtuel " + Integer.toString(nombreJoueursVirtuels), tabPropCards.pop(),
						tabPropCards.pop(), this));
				System.out.println("Joueur virtuel ajouté !");
			} else {
				nombreJoueursReels++;
				tabJoueurs.add(new Reel(reponse, tabPropCards.pop(), tabPropCards.pop(), this));
				System.out.println(reponse + " ajouté à la partie !");
			}
			this.sendMessage("UpdateJoueurs");
		}
		carteMilieu = tabPropCards.pop();
		this.sendMessage("FinInit");

		console.addObserver(tabJoueurs.get(0));
		console.addObserver(tabJoueurs.get(1));
		console.addObserver(tabJoueurs.get(2));

		// Création de l'interface graphique du jeu
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new InterfacePlateau(p);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		// Laisse le temps à l'interface de se lancer avant d'éxécuter la suite
		try {
			Thread.sleep(800);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Notifie les Observateurs avec un message
	 * 
	 * @param string
	 *            Message à envoyer aux Observateurs
	 */
	public void sendMessage(String string) {
		this.setChanged();
		this.notifyObservers(string);
	}

	/**
	 * Renvoie la Pile liée à la Partie
	 * 
	 * @return Pile de la Partie
	 */
	public Pile getPile() {
		return this.pile;
	}

	/**
	 * Mets le Thread courant en pause, qui sera réveillé lorsqu'une réponse aura
	 * été envoyée par un Contrôleur ou la Console via un Notify
	 */
	private synchronized void recupererInput() {
		try {
			this.wait();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Renvoie le tableau contenant les joueurs
	 * 
	 * @return Tableau des joueurs de la Partie
	 */
	public ArrayList<Joueur> getJoueurs() {
		return this.tabJoueurs;
	}

	/**
	 * @return Booléen<br>
	 *         true = variante 1<br>
	 *         false = pas variante 1
	 */
	public boolean getVariante1() {
		return this.variante1;
	}

	/**
	 * @return Booléen<br>
	 *         true = variante 2<br>
	 *         false = pas variante 2
	 */
	public boolean getVariante2() {
		return this.variante2;
	}

	/**
	 * Définit la variable réponse avec un String en paramètre
	 * 
	 * @param reponse
	 *            Chaîne de caractère à définir dans la variable reponse
	 */
	public void setReponse(String reponse) {
		this.reponse = reponse;
	}

	/**
	 * Retourne le joueur relatif qui doit jouer dans x tours
	 * 
	 * @param x
	 *            Le nombre de tours qui doit défiler
	 * @return 0, 1 ou 2, pour cibler le joueur dans tabJoueurs
	 */
	public Integer getNextRelativePlayer(Integer x) {
		Integer tour = joueurEnCours + x;
		while (tour > 2) {
			tour = tour - 3;
		}
		return tour;
	}

	/**
	 * Retourne le joueur en cours
	 * 
	 * @return Index du joueur en cours
	 */
	public Integer getJoueurEnCours() {
		return this.joueurEnCours;
	}

	/**
	 * Réalise un tour complet de Partie<br>
	 * Comprend les interactions des joueurs, les affichages etc...
	 */
	public void tourSuivant() {
		if (!pile.isThereATrickVisible()) {
			if (pioche.piocherTrick(pile))
				;
			else {
				finPartie = true;
				return;
			}
		}

		// On affiche le joueur en cours
		this.sendMessage("AffichageTour");
		System.out.println("------ Tour de " + tabJoueurs.get(joueurEnCours).name + " ("
				+ tabJoueurs.get(joueurEnCours).getScore() + " points) ------");

		// On appelle le déroulement d'un tour
		tabJoueurs.get(joueurEnCours).afficherTour(this);

		// Passage au tour suivant
		tabJoueurs.get(joueurEnCours).setChosenTrick(false);
		joueurEnCours = getNextRelativePlayer(1);
		this.tour += 1;
	}

	/**
	 * Fait l'état de la partie avant le tour suivant<br>
	 * Si finPartie = true, on termine la partie
	 */
	public void etatPartie() {
		Boolean arretVolontaire = false;
		if (finPartie == false && theOtherHatTrickCount < 3) {
			this.sendMessage("AskTourSuivant");
			reponse = "";
			while (!(reponse.equalsIgnoreCase("Oui")) && !(reponse.equalsIgnoreCase("Non"))) {
				System.out.println("Voulez-vous jouer le tour suivant ? Tapez Oui ou Non");
				recupererInput();
			}

			if (reponse.equalsIgnoreCase("Non")) {
				finPartie = true;
				arretVolontaire = true;
			} else {
				System.out.println("Bien, continuons :");
			}
		} else if (theOtherHatTrickCount > 2) {
			penaltyTheOtherHatTrick();
			finPartie = true;
		}

		if (finPartie == true) {
			System.out.println("Fin de la partie !");
			System.out.println("Scores des joueurs :");
			Iterator<Joueur> it = tabJoueurs.iterator();
			vainqueur = tabJoueurs.get(0);
			while (it.hasNext()) {
				Joueur j = it.next();
				System.out.println(j.name + " : " + j.getScore() + " points");
				if (j.getScore() > vainqueur.getScore()) {
					vainqueur = j;
				}
			}
			System.out.println(vainqueur.name + " remporte la partie !");
			if (this.theOtherHatTrickCount < 3 && arretVolontaire == false) {
				this.sendMessage("FinPartieSuccess");
			} else if (arretVolontaire == false) {
				this.sendMessage("FinPartieFail");
			} else {
				this.sendMessage("ArretVolontaire");
			}
			System.out.println("Fermez la fenêtre ou tapez quelque chose pour arrêter le programme...");
			recupererInput();
			System.exit(0);
		}
	}

	/**
	 * Inflige les pénalités aux joueurs possédant The Hat et The Other Rabbit
	 */
	public void penaltyTheOtherHatTrick() {
		System.out.println(
				"Les 3 joueurs ont tenté The Other Hat Trick, en vain. Une pénalité est attribuée aux joueurs possédant The Hat et The Other Rabbit.");
		Iterator<Joueur> it = tabJoueurs.iterator();
		while (it.hasNext()) {
			Joueur j = it.next();
			if (j.carteUne.getProp().equalsIgnoreCase("The Hat") || j.carteDeux.getProp().equalsIgnoreCase("The Hat")) {
				j.addPoints(-3, 0);
				System.out.println(j.name + " perd 3 points à cause de The Hat !");
			}
			if (j.carteUne.getProp().equalsIgnoreCase("The Other Rabbit")
					|| j.carteDeux.getProp().equalsIgnoreCase("The Other Rabbit")) {
				j.addPoints(-3, 0);
				System.out.println(j.name + " perd 3 points à cause de The Other Rabbit !");
			}
		}
	}

	/**
	 * Retourne le nombre de tours déjà passés
	 * 
	 * @return Nombre de tours passés depuis le début de la Partie
	 */
	public Integer getNombreDeTours() {
		return this.tour;
	}

	/**
	 * Retourne le joueur vainqueur
	 * 
	 * @return Vainqueur de la Partie
	 */
	public Joueur getVainqueur() {
		return this.vainqueur;
	}

	/**
	 * Permet à un joueur de changer une de ses cartes avec celle du milieu
	 * 
	 * @param joueur
	 *            Joueur qui doit échanger sa carte
	 * @param cote
	 *            Carte du Joueur qui doit être échangée ("Droite" ou "Gauche")
	 */
	public void changerCarteMilieu(Joueur joueur, String cote) {
		PropCard carte1 = null;
		if (cote.equalsIgnoreCase("Droite")) {
			carte1 = joueur.carteDeux;
			joueur.carteDeux = carteMilieu;
			carteMilieu = carte1;
		} else {
			carte1 = joueur.carteUne;
			joueur.carteUne = carteMilieu;
			carteMilieu = carte1;
		}
		joueur.setChosenTrick(true);
	}

	/**
	 * Déroulement de la partie
	 * 
	 * @param args
	 *            Arguments Main
	 */
	public static void main(String[] args) {
		Partie p = new Partie();
		while (p.finPartie == false) {
			p.tourSuivant();
			p.etatPartie();
		}
	}

	/**
	 * Notifie tous les observateurs qu'une réponse a été reçue
	 * 
	 * @param obs
	 *            Observable qui a envoyé la notification
	 * 
	 * @param arg
	 *            Objet envoyé par l'Observable
	 */
	@Override
	public synchronized void update(Observable obs, Object arg) {
		if (obs instanceof ConsoleInput || obs instanceof ControleurOuiNon || obs instanceof ControleurPlateau) {
			this.reponse = (String) arg;
			this.notifyAll();
		}
	}

}

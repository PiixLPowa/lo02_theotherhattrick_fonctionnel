package fr.lo02.bessonpeuziat.controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.Observable;

import javax.swing.JButton;

import fr.lo02.bessonpeuziat.modele.Joueur;
import fr.lo02.bessonpeuziat.modele.Partie;

/**
 * ControleurPlateau<br>
 * Controleur responsable de toutes les actions réalisées dans la Vue
 * InterfacePlateau pendant le déroulement du jeu.
 */

public class ControleurPlateau extends Observable {

	/**
	 * Crée une instance de ControleurPlateau
	 * 
	 * @param p
	 *            Partie en cours
	 * @param btnj11
	 *            Bouton carte Gauche Joueur 1
	 * @param btnj12
	 *            Bouton carte Droite Joueur 1
	 * @param btnj21
	 *            Bouton carte Gauche Joueur 2
	 * @param btnj22
	 *            Bouton carte Droite Joueur 2
	 * @param btnj31
	 *            Bouton carte Gauche Joueur 3
	 * @param btnj32
	 *            Bouton carte Droite Joueur 3
	 * @param btnOui
	 *            Bouton "Oui"
	 * @param btnNon
	 *            Bouton "Non"
	 */
	public ControleurPlateau(Partie p, JButton btnj11, JButton btnj12, JButton btnj21, JButton btnj22, JButton btnj31,
			JButton btnj32, JButton btnOui, JButton btnNon) {
		this.addObserver(p);
		Iterator<Joueur> it = p.getJoueurs().iterator();
		while (it.hasNext()) {
			this.addObserver(it.next());
		}

		btnj11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (p.getJoueurEnCours() == 0)
					;
				else {
					setChanged();
					notifyObservers("0");
					try {
						Thread.sleep(50);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				setChanged();
				notifyObservers("Gauche");
			}
		});

		btnj12.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (p.getJoueurEnCours() == 0)
					;
				else {
					setChanged();
					notifyObservers("0");
					try {
						Thread.sleep(50);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				setChanged();
				notifyObservers("Droite");
			}
		});

		btnj21.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (p.getJoueurEnCours() == 1)
					;
				else {
					setChanged();
					notifyObservers("1");
					try {
						Thread.sleep(50);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				setChanged();
				notifyObservers("Gauche");
			}
		});

		btnj22.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (p.getJoueurEnCours() == 1)
					;
				else {
					setChanged();
					notifyObservers("1");
					try {
						Thread.sleep(50);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				setChanged();
				notifyObservers("Droite");
			}
		});

		btnj31.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (p.getJoueurEnCours() == 2)
					;
				else {
					setChanged();
					notifyObservers("2");
					try {
						Thread.sleep(50);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				setChanged();
				notifyObservers("Gauche");
			}
		});

		btnj32.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (p.getJoueurEnCours() == 2)
					;
				else {
					setChanged();
					notifyObservers("2");
					try {
						Thread.sleep(50);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				setChanged();
				notifyObservers("Droite");
			}
		});

		btnOui.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setChanged();
				notifyObservers("Oui");
			}
		});

		btnNon.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setChanged();
				notifyObservers("Non");
			}
		});
	}

}

package fr.lo02.bessonpeuziat.modele;

/**
 * Représente une carte au sens large (Prop ou Trick)<br>
 * On ne crée que des objets spécialisés, par des objets Carte
 */

public class Carte {

	/**
	 * Etat de visibilité d'une carte
	 */
	protected boolean visible;

	/**
	 * Crée une instance de Carte<br>
	 * Une carte est cachée par défaut.
	 */
	public Carte() {
		this.visible = false;
	}

	/**
	 * Renvoie l'état de visibilité d'une carte sous forme de Boolean
	 * 
	 * @return Etat de visibilité d'une carte (true = visible, false = cachée)
	 */
	public boolean isVisible() {
		return visible;
	}

	/**
	 * Renvoie l'état de visibilité d'une carte sous forme de String
	 * 
	 * @return Etat de visibilité d'une carte sous forme de String ("Visible" ou
	 *         "Cachée")
	 */
	public String getVisibleString() {
		if (this.isVisible()) {
			return ("Visible");
		} else {
			return ("Cachée");
		}
	}

	/**
	 * Méthode qui défini l'état de visibilité d'une carte
	 * 
	 * @param etat
	 *            L'état à définir
	 */
	public void setVisible(boolean etat) {
		this.visible = etat;
	}

}

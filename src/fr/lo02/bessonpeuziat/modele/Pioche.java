package fr.lo02.bessonpeuziat.modele;

import java.util.*;

/**
 * Représente une Pioche<br>
 * La Pioche contient les Tricks qui seront ajoutés à la pile au fur et à
 * mesure, et propose des méthodes pour interagir avec.
 */

public class Pioche {
	private static LinkedList<Trick> tricksPioche = new LinkedList<Trick>();

	/**
	 * Crée une instance de Pioche
	 */
	public Pioche() {
	}

	/**
	 * Ajoute un Trick à la Pioche
	 * 
	 * @param trick
	 *            Trick à ajouter à la Pioche
	 */
	public void addTrickToPioche(Trick trick) {
		tricksPioche.add(trick);
	}

	/**
	 * Renvoie le nombre de cartes dans la Pioche
	 * 
	 * @return Le nombre de cartes restantes dans la Pioche
	 */
	public Integer getPiocheSize() {
		return tricksPioche.size();
	}

	/**
	 * Renvoie true si on peut piocher une carte et la mettre dans la Pile, et rend
	 * la carte piochée visible
	 * 
	 * @param pile
	 *            Pile de la Partie
	 * 
	 * @return Si il y a possibilité de pioche
	 */
	public boolean piocherTrick(Pile pile) {
		if (tricksPioche.size() > 0) {
			pile.addTrickToPile(tricksPioche.removeLast());
			pile.setLastVisible();
			return true;
		} else {
			return false;
		}
	}
}

package fr.lo02.bessonpeuziat.modele;

/**
 * Enumération de Tricks<br>
 * Liste les différents Tricks disponibles
 */

public enum enumTrick {

	/**
	 * The Other Hat Trick est composé de The Hat et de The Other Rabbit (6 points)
	 */
	TheOtherHatTrick("The Other Hat Trick", enumPropCard.TheHat, enumPropCard.TheHat, enumPropCard.TheOtherRabbit,
			enumPropCard.TheOtherRabbit, 6),

	/**
	 * The Hungry Rabbit est composé de The Rabbit ou The Other Rabbit et de Carrots
	 * ou The Lettuce (1 points)
	 */
	TheHungryRabbit("The Hungry Rabbit", enumPropCard.TheRabbit, enumPropCard.TheOtherRabbit, enumPropCard.Carrots,
			enumPropCard.TheLettuce, 1),

	/**
	 * The Bunch of Carrots est composé de Carrots et de Carrots (2 points)
	 */
	TheBunchOfCarrots("The Bunch of Carrots", enumPropCard.Carrots, enumPropCard.Carrots, enumPropCard.Carrots,
			enumPropCard.Carrots, 2),

	/**
	 * The Vegetable Patch est composé de Carrots et de The Lettuce (3 points)
	 */
	TheVegetablePatch("The Vegetable Patch", enumPropCard.Carrots, enumPropCard.Carrots, enumPropCard.TheLettuce,
			enumPropCard.TheLettuce, 3),

	/**
	 * The Rabbit That Didn't Like Carrots est composé de The Rabbit ou The Other
	 * Rabbit et de The Lettuce (4 points)
	 */
	TheRabbitThatDidntLikeCarrots("The Rabbit That Didn't Like Carrots", enumPropCard.TheRabbit,
			enumPropCard.TheOtherRabbit, enumPropCard.TheLettuce, enumPropCard.TheLettuce, 4),

	/**
	 * The Pair Of Rabbits est composé de The Rabbit et de The Other Rabbit (5
	 * points)
	 */
	ThePairOfRabbits("The Pair Of Rabbits", enumPropCard.TheRabbit, enumPropCard.TheRabbit, enumPropCard.TheOtherRabbit,
			enumPropCard.TheOtherRabbit, 5),

	/**
	 * The Vegetable Hat Trick est composé de The Hat et de Carrots ou The Lettuce
	 * (2 points)
	 */
	TheVegetableHatTrick("The Vegetable Hat Trick", enumPropCard.TheHat, enumPropCard.TheHat, enumPropCard.Carrots,
			enumPropCard.TheLettuce, 2),

	/**
	 * The Carrot Hat Trick est composé de The Hat et de Carrots (3 points)
	 */
	TheCarrotHatTrick("The Carrot Hat Trick", enumPropCard.TheHat, enumPropCard.TheHat, enumPropCard.Carrots,
			enumPropCard.Carrots, 3),

	/**
	 * The Slightly Easier Hat Trick est composé de The Hat et de The Rabbit ou The
	 * Other Rabbit (4 points)
	 */
	TheSlightlyEasierHatTrick("The Slightly Easier Hat Trick", enumPropCard.TheHat, enumPropCard.TheHat,
			enumPropCard.TheRabbit, enumPropCard.TheOtherRabbit, 4),

	/**
	 * The Hat Trick est composé de The Hat et de The Rabbit (5 points)
	 */
	TheHatTrick("The Hat Trick", enumPropCard.TheHat, enumPropCard.TheHat, enumPropCard.TheRabbit,
			enumPropCard.TheRabbit, 5),

	/**
	 * The Dead Of Rabbit est composé de The Carnivorous Rabbit et de The Other
	 * Rabbit ou The Rabbit (5 points)
	 */
	TheDeadOfRabbit("The Dead Of Rabbit", enumPropCard.TheCarnivorousRabbit, enumPropCard.TheCarnivorousRabbit,
			enumPropCard.TheRabbit, enumPropCard.TheOtherRabbit, 5),

	/**
	 * The Thug Rabbit est composé de The Cap et de The Other Rabbit ou The Rabbit
	 * (4 points)
	 */
	TheThugRabbit("The Thug Rabbit", enumPropCard.TheCap, enumPropCard.TheCap, enumPropCard.TheRabbit,
			enumPropCard.TheOtherRabbit, 4),

	/**
	 * The Head Gear est composé de The Cap et de The Hat (1 point)
	 */
	TheHeadGear("The Head Gear", enumPropCard.TheCap, enumPropCard.TheCap, enumPropCard.TheHat, enumPropCard.TheHat, 1);

	/**
	 * Nom du Trick
	 */
	private String name = "";

	/**
	 * Tableau de PropCard du côté Gauche du Trick
	 */
	private PropCard[] leftHand = new PropCard[2];

	/**
	 * Tableau de PropCard du côté Droite du Trick
	 */
	private PropCard[] rightHand = new PropCard[2];

	/**
	 * Valeur du Trick
	 */
	private int valeur;

	/**
	 * Constructeur de enumTrick
	 * 
	 * @param name
	 *            Le nom du Trick
	 * @param left1
	 *            PropCard 1 du côté Gauche du Trick
	 * @param left2
	 *            PropCard 2 du côté Gauche du Trick
	 * @param right1
	 *            PropCard 1 du côté Droite du Trick
	 * @param right2
	 *            PropCard 2 du côté Droite du Trick
	 * @param bonus
	 *            Valeur du Trick
	 */
	private enumTrick(String name, enumPropCard left1, enumPropCard left2, enumPropCard right1, enumPropCard right2,
			int bonus) {
		this.name = name;
		this.leftHand[0] = new PropCard(left1);
		this.leftHand[1] = new PropCard(left2);
		this.rightHand[0] = new PropCard(right1);
		this.rightHand[1] = new PropCard(right2);
		this.valeur = bonus;
	}

	/**
	 * Retourne le nom du Trick
	 * 
	 * @return Nom du Trick
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Renvoie les PropCard de Gauche nécéssaires à réaliser le Trick
	 * 
	 * @return Tableau de PropCard du côté Gauche du Trick
	 */
	public PropCard[] getPropCardLeft() {
		return leftHand;
	}

	/**
	 * Renvoie les PropCard de Droite nécéssaires à réaliser le Trick
	 * 
	 * @return Tableau de PropCard du côté Droite du Trick
	 */
	public PropCard[] getPropCardRight() {
		return rightHand;
	}

	/**
	 * Renvoie le nombre de points que rapporte un Trick
	 * 
	 * @return Valeur du Trick
	 */
	public int getValeur() {
		return this.valeur;
	}
}
package fr.lo02.bessonpeuziat.modele;

import java.util.*;

/**
 * Représente une pile du jeu<br>
 * Une Pile contient les Tricks présents dans la Pile et propose des méthodes
 * pour interagir avec.
 */

public class Pile {
	/**
	 * Tableau contenant les cartes de la Pile
	 */
	private LinkedList<Trick> tricksPile = new LinkedList<Trick>();

	/**
	 * Crée une instance de Pile
	 */
	public Pile() {
	}

	/**
	 * Ajoute un Trick à la Pile
	 * 
	 * @param trick
	 *            Le Trick que l'on souhaite ajouter à la pile
	 */
	public void addTrickToPile(Trick trick) {
		tricksPile.add(trick);
	}

	/**
	 * Renvoie si il y a un Trick visible en haut de la Pile ou non
	 * 
	 * @return Booléen<br>
	 *         true = Il y a un Trick visible en haut de la Pile
	 */
	public boolean isThereATrickVisible() {
		if (tricksPile.size() > 0) {
			return tricksPile.getLast().isVisible();
		} else {
			return false;
		}
	}

	/**
	 * Rend la dernière carte de la Pile visible
	 */
	public void setLastVisible() {
		if (tricksPile.size() > 0) {
			tricksPile.getLast().setVisible(true);
		}
	}

	/**
	 * Renvoie le Trick en haut de la Pile
	 * 
	 * @return Trick en haut de la Pile
	 */
	public Trick getTrick() {
		return tricksPile.getLast();
	}

	/**
	 * Supprime le Trick en haut de la pile
	 */
	public void removeLastTrick() {
		tricksPile.removeLast();
	}
}

package fr.lo02.bessonpeuziat.modele;

/**
 * Représente un Joueur Virtuel
 */

public class Virtuel extends Joueur {

	/**
	 * Crée une instance d'un Joueur Virtuel
	 * 
	 * @param name
	 *            Le nom du joueur
	 * @param carteUne
	 *            Une PropCard, qui sera la carte Gauche du joueur
	 * @param carteDeux
	 *            Une PropCard, qui sera la carte Droite du joueur
	 * @param p
	 *            Partie en cours
	 */
	public Virtuel(String name, PropCard carteUne, PropCard carteDeux, Partie p) {
		super(name, carteUne, carteDeux, p);
	}

	/**
	 * Retourne une carte du Joueur Virtuel
	 * 
	 * @param j
	 *            Joueur qui doit retourner une carte
	 */
	public void penaltyTrick(Joueur j, Partie p) {
		if (!j.carteUne.isVisible() && !j.carteDeux.isVisible()) {
			if ((Math.random() * 100) <= 50) {
				j.carteDeux.setVisible(true);
				System.out.println(j.name + " a retourné sa carte Droite !");
			} else {
				j.carteUne.setVisible(true);
				System.out.println(j.name + " a retourné sa carte Gauche !");
			}
		} else if (j.carteUne.isVisible() && !j.carteDeux.isVisible()) {
			j.carteDeux.setVisible(true);
			System.out.println(j.name + " a retourné sa carte Droite !");
		} else if (!j.carteUne.isVisible() && j.carteDeux.isVisible()) {
			j.carteUne.setVisible(true);
			System.out.println(j.name + " a retourné sa carte Gauche !");
		} else {
			System.out.println(
					"Les deux cartes de " + j.name + " sont déjà visibles, aucune pénalité ne lui est infligée.");
		}
	}

	/**
	 * Gère l'affichage, les messages, la mécanique d'un tour complet pour un Joueur
	 * Virtuel
	 * 
	 * @param p
	 *            Partie en cours
	 */
	public void afficherTour(Partie p) {

		String cote1 = "Droite";
		String cote2;
		Joueur joueur2;

		// On affiche le Trick déjà retourné dans la pile

		System.out.println("Trick proposé : " + p.pile.getTrick().toString());

		// Si le joueur a une carte en commun avec le Trick il conserve le Trick sinon
		// il change
		if (p.tabJoueurs.get(p.joueurEnCours).carteUne.getProp()
				.equals(p.pile.getTrick().getPropCardLeft()[0].getProp())
				|| p.tabJoueurs.get(p.joueurEnCours).carteUne.getProp()
						.equals(p.pile.getTrick().getPropCardLeft()[1].getProp())
				|| p.tabJoueurs.get(p.joueurEnCours).carteUne.getProp()
						.equals(p.pile.getTrick().getPropCardRight()[0].getProp())
				|| p.tabJoueurs.get(p.joueurEnCours).carteUne.getProp()
						.equals(p.pile.getTrick().getPropCardRight()[1].getProp())) {
			p.tabJoueurs.get(p.joueurEnCours).setChosenTrick(true);
			cote1 = "Droite";
			if (p.pile.getTrick().getName() != "The Other Hat Trick" && p.pioche.getPiocheSize() > 0) {
				this.sendMessage("OrdiGardeTrick");
				System.out.println("L'ordinateur conserve le Trick.");
			}
		} else if (p.tabJoueurs.get(p.joueurEnCours).carteDeux.getProp()
				.equals(p.pile.getTrick().getPropCardLeft()[0].getProp())
				|| p.tabJoueurs.get(p.joueurEnCours).carteDeux.getProp()
						.equals(p.pile.getTrick().getPropCardLeft()[1].getProp())
				|| p.tabJoueurs.get(p.joueurEnCours).carteDeux.getProp()
						.equals(p.pile.getTrick().getPropCardRight()[0].getProp())
				|| p.tabJoueurs.get(p.joueurEnCours).carteDeux.getProp()
						.equals(p.pile.getTrick().getPropCardRight()[1].getProp())) {
			p.tabJoueurs.get(p.joueurEnCours).setChosenTrick(true);
			cote1 = "Gauche";
			if (p.pile.getTrick().getName() != "The Other Hat Trick" && p.pioche.getPiocheSize() > 0) {
				this.sendMessage("OrdiGardeTrick");
				System.out.println("L'ordinateur conserve le Trick.");
			}
		} else if (p.pile.getTrick().getName() != "The Other Hat Trick" && p.pioche.getPiocheSize() > 0) {
			p.tabJoueurs.get(p.joueurEnCours).changeTrick(p.pile, p.pioche);
			this.sendMessage("OrdiChangeTrick");
			System.out.println("L'ordinateur change de Trick.");
			if (p.tabJoueurs.get(p.joueurEnCours).carteUne.getProp()
					.equals(p.pile.getTrick().getPropCardLeft()[0].getProp())
					|| p.tabJoueurs.get(p.joueurEnCours).carteUne.getProp()
							.equals(p.pile.getTrick().getPropCardLeft()[1].getProp())
					|| p.tabJoueurs.get(p.joueurEnCours).carteUne.getProp()
							.equals(p.pile.getTrick().getPropCardRight()[0].getProp())
					|| p.tabJoueurs.get(p.joueurEnCours).carteUne.getProp()
							.equals(p.pile.getTrick().getPropCardRight()[1].getProp())) {
				cote1 = "Droite";
			} else if (p.tabJoueurs.get(p.joueurEnCours).carteDeux.getProp()
					.equals(p.pile.getTrick().getPropCardLeft()[0].getProp())
					|| p.tabJoueurs.get(p.joueurEnCours).carteDeux.getProp()
							.equals(p.pile.getTrick().getPropCardLeft()[1].getProp())
					|| p.tabJoueurs.get(p.joueurEnCours).carteDeux.getProp()
							.equals(p.pile.getTrick().getPropCardRight()[0].getProp())
					|| p.tabJoueurs.get(p.joueurEnCours).carteDeux.getProp()
							.equals(p.pile.getTrick().getPropCardRight()[1].getProp())) {
				cote1 = "Gauche";
			} else {
				if ((Math.random() * 100) <= 50) {
					cote1 = "Droite";
				} else {
					cote1 = "Gauche";
				}
			}
		} else {
			p.tabJoueurs.get(p.joueurEnCours).setChosenTrick(true);
			if (p.tabJoueurs.get(p.joueurEnCours).carteUne.getProp()
					.equals(p.pile.getTrick().getPropCardLeft()[0].getProp())
					|| p.tabJoueurs.get(p.joueurEnCours).carteUne.getProp()
							.equals(p.pile.getTrick().getPropCardLeft()[1].getProp())
					|| p.tabJoueurs.get(p.joueurEnCours).carteUne.getProp()
							.equals(p.pile.getTrick().getPropCardRight()[0].getProp())
					|| p.tabJoueurs.get(p.joueurEnCours).carteUne.getProp()
							.equals(p.pile.getTrick().getPropCardRight()[1].getProp())) {
				cote1 = "Droite";
			} else if (p.tabJoueurs.get(p.joueurEnCours).carteDeux.getProp()
					.equals(p.pile.getTrick().getPropCardLeft()[0].getProp())
					|| p.tabJoueurs.get(p.joueurEnCours).carteDeux.getProp()
							.equals(p.pile.getTrick().getPropCardLeft()[1].getProp())
					|| p.tabJoueurs.get(p.joueurEnCours).carteDeux.getProp()
							.equals(p.pile.getTrick().getPropCardRight()[0].getProp())
					|| p.tabJoueurs.get(p.joueurEnCours).carteDeux.getProp()
							.equals(p.pile.getTrick().getPropCardRight()[1].getProp())) {
				cote1 = "Gauche";
			} else {
				if ((Math.random() * 100) <= 50) {
					cote1 = "Droite";
				} else {
					cote1 = "Gauche";
				}
			}
		}

		System.out.println("Trick à jouer : " + p.pile.getTrick().toString());

		if ((Math.random() * 100) <= 50) {
			joueur2 = p.tabJoueurs.get(p.getNextRelativePlayer(1));
		} else {
			joueur2 = p.tabJoueurs.get(p.getNextRelativePlayer(2));
		}

		if ((Math.random() * 100) <= 50) {
			cote2 = "Droite";
		} else {
			cote2 = "Gauche";
		}

		p.tabJoueurs.get(p.joueurEnCours).changeProp(p.tabJoueurs.get(p.joueurEnCours), joueur2, cote1, cote2);

		this.sendMessage("OrdiChangedProp-" + cote1 + "-" + cote2 + "-" + joueur2.name);
		System.out.println(p.tabJoueurs.get(p.joueurEnCours).name + " a échangé sa carte " + cote1 + " avec la carte "
				+ cote2 + " de " + joueur2.name);

		// On teste si le Trick est validé ou non
		if (p.tabJoueurs.get(p.joueurEnCours).tenterTrick(p.pile.getTrick())) {
			if (p.pile.getTrick().getName().equalsIgnoreCase("The Other Hat Trick")) {
				p.finPartie = true;
			}
			System.out.println(
					"Ta-Daaaah ! L'ordinateur a réussi le Trick ! ( + " + p.pile.getTrick().getValue() + " points)");
			this.tourVariante -= 1;
			System.out.println("Les cartes utilisées sont :");
			System.out.println(p.tabJoueurs.get(p.joueurEnCours).carteUne.getProp() + " et "
					+ p.tabJoueurs.get(p.joueurEnCours).carteDeux.getProp());

			if (p.getVariante1() == false && p.getVariante2() == false) {
				p.tabJoueurs.get(p.joueurEnCours).addPoints(p.pile.getTrick().getValue(), 0);
			} else if (p.getVariante1() == true && p.getVariante2() == false) {
				p.tabJoueurs.get(p.joueurEnCours).addPoints(p.pile.getTrick().getValue(), this.tourVariante);
			} else if (p.getVariante1() == false && p.getVariante2() == true) {
				p.tabJoueurs.get(p.joueurEnCours).addPoints(p.pile.getTrick().getValue(), p.getNombreDeTours());
			}
			this.sendMessage("OrdiSuccessTrick-" + p.pile.getTrick().getValue());
			p.pile.removeLastTrick();

			// On décide quelle carte va être placée au milieu
			if ((Math.random() * 100) < 50) {
				p.reponse = "Droite";
			} else {
				p.reponse = "Gauche";
			}

			// On échange la carte et on les cache toutes
			System.out.println("L'ordinateur a échangé une de ses cartes contre celle du milieu !");
			p.changerCarteMilieu(p.tabJoueurs.get(p.joueurEnCours), p.reponse);
			p.carteMilieu.setVisible(false);
			p.tabJoueurs.get(p.joueurEnCours).carteUne.setVisible(false);
			p.tabJoueurs.get(p.joueurEnCours).carteDeux.setVisible(false);
		} else {
			if (p.pile.getTrick().getName().equalsIgnoreCase("The Other Hat Trick")) {
				p.theOtherHatTrickCount++;
			}
			this.sendMessage("OrdiFailedTrick");
			System.out.println("L'ordinateur a raté son Trick.");
			p.tabJoueurs.get(p.joueurEnCours).penaltyTrick(p.tabJoueurs.get(p.joueurEnCours), p);
		}
		this.sendMessage("UpdateCartes");
	}

}

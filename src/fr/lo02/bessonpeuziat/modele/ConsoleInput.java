package fr.lo02.bessonpeuziat.modele;

import java.util.Observable;
import java.util.Scanner;

/**
 * ConsoleInput<br>
 * Cette classe permet de lancer un Scanner dans un Thread séparé afin de ne pas
 * bloquer le déroulement du jeu
 */

public class ConsoleInput extends Observable implements Runnable {
	Thread thread;
	boolean isRunning;

	/**
	 * Crée une instance de ConsoleInput
	 * 
	 * @param p
	 *            Observateur de la console, ici ce sera une Partie, ajoutée lors de
	 *            la construction de l'objet.
	 */
	public ConsoleInput(Partie p) {
		this.addObserver(p);
		thread = new Thread(this);
	}

	/**
	 * Méthode appelée lors du démarrage du thread. Il demande en permanence à
	 * l'utilisateur une entrée clavier, qui est ensuite transmise via une
	 * notification au modèle.
	 */
	public void run() {
		Scanner input = new Scanner(System.in);

		while (isRunning) {
			String line = input.nextLine();

			// Notification à la vue que l'utilisateur a entré quelque chose.
			setChanged();
			notifyObservers(line);
		}
		input.close();
	}

	/**
	 * Lance le thread.
	 */
	public void demarrer() {
		this.isRunning = true;
		thread.start();
	}

	/**
	 * Arrête le Thread du Scanner
	 */
	public void arreter() {
		this.isRunning = false;
	}
}

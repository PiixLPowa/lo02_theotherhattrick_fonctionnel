package fr.lo02.bessonpeuziat.modele;

import java.util.Observable;
import java.util.Observer;

import fr.lo02.bessonpeuziat.controleur.ControleurOuiNon;
import fr.lo02.bessonpeuziat.controleur.ControleurPlateau;

/**
 * Représente un joueur<br>
 * Un joueur est soit réel soit virtuel, les classes Reel et Virtuel héritent
 * donc de Joueur
 */

public abstract class Joueur extends Observable implements Observer {

	/**
	 * Variable dans laquelle on stock les instructions de l'utilisateur
	 */
	protected String reponse;

	/**
	 * Désigne la carte Gauche du joueur
	 */
	protected PropCard carteUne;

	/**
	 * Désigne la carte Droite du joueur
	 */
	protected PropCard carteDeux;

	/**
	 * Nom du joueur
	 */
	protected String name;

	/**
	 * Score du joueur
	 */
	private Integer score;

	/**
	 * Booléen qui défini si le joueur a choisi le Trick à réaliser au cours du tour
	 */
	private boolean hasChosenTrick;

	/**
	 * Compteur de tour de variante
	 */
	protected Integer tourVariante = 0;

	/**
	 * Constructeur de Joueur
	 * 
	 * @param name
	 *            Le nom du joueur
	 * @param carteUne
	 *            Une PropCard, qui sera la carte Gauche du joueur
	 * @param carteDeux
	 *            Une PropCard, qui sera la carte Droite du joueur
	 * @param p
	 *            Partie en cours
	 */
	public Joueur(String name, PropCard carteUne, PropCard carteDeux, Partie p) {
		this.name = name;
		this.score = 0;
		this.hasChosenTrick = false;
		this.carteUne = carteUne;
		this.carteDeux = carteDeux;
	}

	/**
	 * Notifie les Observateurs avec un message
	 * 
	 * @param string
	 *            Message à envoyer aux Observateurs
	 */
	public void sendMessage(String string) {
		this.setChanged();
		this.notifyObservers(string);
	}

	/**
	 * Mets le Thread courant en pause, qui sera réveillé lorsqu'une réponse aura
	 * été envoyée par un Contrôleur ou la Console via un Notify
	 */
	protected synchronized void recupererInput() {
		try {
			this.wait();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Méthode pour savoir si le Joueur a choisi son Trick durant le tour
	 * 
	 * @return Booléen (true = a choisi le Trick, false = n'a pas encore décidé)
	 */
	public boolean hasChosenTrick() {
		return this.hasChosenTrick;
	}

	/**
	 * Renvoie le pseudo du joueur
	 * 
	 * @return Le nom du joueur
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Ajoute des points à un joueur
	 * 
	 * @param points
	 *            Nombre de points à ajouter au joueur
	 * @param bonus
	 *            Nombre de points supplémentaires (bonus) à ajouter au joueur
	 */
	public void addPoints(Integer points, Integer bonus) {
		this.score = this.score + (points + bonus);
	}

	/**
	 * Appelle la fonction enfant pour afficher le tour du joueur
	 * 
	 * @param p
	 *            Partie en cours
	 */
	public abstract void afficherTour(Partie p);

	/**
	 * Tente le Trick fourni en paramètre avec les deux cartes du joueur
	 * 
	 * @param trick
	 *            Le Trick à tenter
	 * @return Booléen (true = Le Trick est réalisé, false = Le Trick n'est pas
	 *         réalisable)
	 */
	public boolean tenterTrick(Trick trick) {
		this.hasChosenTrick = false;
		Boolean valid = false;
		if ((trick.getPropCardLeft()[0].getProp().equals(this.carteUne.getProp())
				|| trick.getPropCardLeft()[1].getProp().equals(this.carteUne.getProp()))) {
			if (trick.getPropCardRight()[0].getProp().equals(this.carteDeux.getProp())
					|| trick.getPropCardRight()[1].getProp().equals(this.carteDeux.getProp())) {
				valid = true;
			}
		} else if ((trick.getPropCardRight()[0].getProp().equals(this.carteUne.getProp())
				|| trick.getPropCardRight()[1].getProp().equals(this.carteUne.getProp()))) {
			if (trick.getPropCardLeft()[0].getProp().equals(this.carteDeux.getProp())
					|| trick.getPropCardLeft()[1].getProp().equals(this.carteDeux.getProp())) {
				valid = true;
			}
		} else {
			valid = false;
		}
		return valid;
	}

	/**
	 * Permet au joueur de piocher un nouveau Trick
	 * 
	 * @param pile
	 *            Pile du jeu
	 * @param pioche
	 *            Pioche du jeu
	 */
	public void changeTrick(Pile pile, Pioche pioche) {
		if (pioche.piocherTrick(pile) && this.hasChosenTrick == false) {
			this.hasChosenTrick = true;
		} else {
			System.out.println("Il n'y a plus de Trick disponible dans la pioche !");
		}
	}

	/**
	 * Définit l'état du Booléen hasChosenTrick
	 * 
	 * @param bool
	 *            Etat à définir (true ou false)
	 */
	public void setChosenTrick(Boolean bool) {
		this.hasChosenTrick = bool;
	}

	/**
	 * Echanger 2 cartes entre 2 joueurs
	 * 
	 * @param joueur1
	 *            Joueur 1
	 * @param joueur2
	 *            Joueur 2
	 * @param cote1
	 *            String qui doit être soit "Droite" soit "Gauche" - Carte à
	 *            échanger du Joueur 1
	 * @param cote2
	 *            String qui doit être soit "Droite" soit "Gauche" - Carte à
	 *            échanger du Joueur 2
	 */
	public void changeProp(Joueur joueur1, Joueur joueur2, String cote1, String cote2) {
		PropCard carte1 = new PropCard(null);
		PropCard carte2 = new PropCard(null);
		if (cote1.equalsIgnoreCase("droite")) {
			carte1 = joueur1.carteDeux;
		} else {
			carte1 = joueur1.carteUne;
		}
		if (cote2.equalsIgnoreCase("droite")) {
			carte2 = joueur2.carteDeux;
			joueur2.carteDeux = carte1;
			if (cote1.equalsIgnoreCase("droite")) {
				joueur1.carteDeux = carte2;
			} else {
				joueur1.carteUne = carte2;
			}
		} else {
			carte2 = joueur2.carteUne;
			joueur2.carteUne = carte1;
			if (cote1.equalsIgnoreCase("droite")) {
				joueur1.carteDeux = carte2;
			} else {
				joueur1.carteUne = carte2;
			}
		}
	}

	/**
	 * Retourne le score du joueur
	 * 
	 * @return Score du joueur
	 */
	public Integer getScore() {
		return this.score;
	}

	/**
	 * Renvoie la carte Gauche du joueur
	 * 
	 * @return PropCard gauche du joueur
	 */
	public PropCard getCarteUne() {
		return this.carteUne;
	}

	/**
	 * Renvoie la carte Droite du joueur
	 * 
	 * @return PropCard droite du joueur
	 */
	public PropCard getCarteDeux() {
		return this.carteDeux;
	}

	/**
	 * Appelle la fonction enfant pour infliger la pénalité d'êchec de Trick au
	 * joueur
	 * 
	 * @param j
	 *            Joueur auquel on inflige la pénalité
	 * @param p
	 *            Partie en cours
	 */
	public abstract void penaltyTrick(Joueur j, Partie p);

	@Override
	public synchronized void update(Observable obs, Object arg) {
		if (obs instanceof ConsoleInput || obs instanceof ControleurOuiNon || obs instanceof ControleurPlateau) {
			this.reponse = (String) arg;
			this.notifyAll();
		}
	}
}

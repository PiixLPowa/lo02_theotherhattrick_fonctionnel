package fr.lo02.bessonpeuziat.modele;

/**
 * Représente un Joueur Réel
 */

public class Reel extends Joueur {

	/**
	 * Crée une instance d'un Joueur Réel
	 * 
	 * @param name
	 *            Le nom du joueur
	 * @param carteUne
	 *            Une PropCard, qui sera la carte Gauche du joueur
	 * @param carteDeux
	 *            Une PropCard, qui sera la carte Droite du joueur
	 * @param p
	 *            Partie en cours
	 */
	public Reel(String name, PropCard carteUne, PropCard carteDeux, Partie p) {
		super(name, carteUne, carteDeux, p);
	}

	/**
	 * Facilite l'affichage des cartes du joueur dans x tours dans la console
	 * 
	 * @param x
	 *            Nombre de tours relatifs au joueur en cours
	 * @param p
	 *            Partie en cours
	 */
	public void afficherCartesJoueur(Integer x, Partie p) {
		System.out.println("-- Cartes de " + p.tabJoueurs.get(p.getNextRelativePlayer(x)).name + " ("
				+ p.tabJoueurs.get(p.getNextRelativePlayer(x)).getScore() + " points) --");
		if (p.tabJoueurs.get(p.getNextRelativePlayer(x)).carteUne.isVisible()) {
			System.out.println("> Gauche : " + p.tabJoueurs.get(p.getNextRelativePlayer(x)).carteUne.getProp());
		} else {
			System.out.println("> Gauche : Cachée");
		}
		if (p.tabJoueurs.get(p.getNextRelativePlayer(x)).carteDeux.isVisible()) {
			System.out.println("> Droite : " + p.tabJoueurs.get(p.getNextRelativePlayer(x)).carteDeux.getProp());
		} else {
			System.out.println("> Droite : Cachée");
		}

	}

	/**
	 * Gère l'affichage, les messages, la mécanique et les Scanners d'un tour
	 * complet pour un Joueur Réel
	 * 
	 * @param p
	 *            Partie en cours
	 */
	public void afficherTour(Partie p) {

		// On affiche les cartes présentes sur le jeu
		System.out.println("> Gauche : " + p.tabJoueurs.get(p.joueurEnCours).carteUne.getProp() + " ("
				+ p.tabJoueurs.get(p.joueurEnCours).carteUne.getVisibleString() + ")");
		System.out.println("> Droite : " + p.tabJoueurs.get(p.joueurEnCours).carteDeux.getProp() + " ("
				+ p.tabJoueurs.get(p.joueurEnCours).carteDeux.getVisibleString() + ")");

		afficherCartesJoueur(1, p);
		afficherCartesJoueur(2, p);

		// On affiche le Trick déjà retourné dans la pile

		System.out.println("Trick proposé : " + p.pile.getTrick().toString());

		// On propose de changer de Trick
		this.sendMessage("TenteTrick");
		while (!p.tabJoueurs.get(p.joueurEnCours).hasChosenTrick()
				&& p.pile.getTrick().getName() != "The Other Hat Trick" && p.pioche.getPiocheSize() > 0) {
			p.reponse = "";
			while (!(p.reponse.equalsIgnoreCase("Oui")) && !(p.reponse.equalsIgnoreCase("Non"))) {
				System.out.println("Voulez vous tenter le trick actuel ? Oui ou Non");
				recupererInput();
			}

			if (p.reponse.equalsIgnoreCase("Non")) {
				p.tabJoueurs.get(p.joueurEnCours).changeTrick(p.pile, p.pioche);
			} else {
				p.tabJoueurs.get(p.joueurEnCours).setChosenTrick(true);
			}

		}

		// Le Trick à jouer est affiché
		this.sendMessage("ChangeTrick");
		System.out.println("Trick à jouer : " + p.pile.getTrick().toString());

		this.sendMessage("ChoixSelfCarte");
		// Le joueur doit changer une de ses cartes
		p.reponse = "";
		while (!(p.reponse.equalsIgnoreCase("Droite")) && !(p.reponse.equalsIgnoreCase("Gauche"))) {
			System.out.println("Choisissez votre carte à échanger (Droite ou Gauche)");
			recupererInput();
		}
		String cote1 = p.reponse;

		this.sendMessage("ChoixOtherCarte");
		p.reponse = "";
		while (!p.reponse.equalsIgnoreCase(Integer.toString(p.getNextRelativePlayer(1)))
				&& !p.reponse.equalsIgnoreCase(Integer.toString(p.getNextRelativePlayer(2)))) {
			System.out.println("Choisissez le joueur (" + p.getNextRelativePlayer(1) + " pour "
					+ p.tabJoueurs.get(p.getNextRelativePlayer(1)).name + " ou " + p.getNextRelativePlayer(2) + " pour "
					+ p.tabJoueurs.get(p.getNextRelativePlayer(2)).name + ")");
			recupererInput();
		}

		Joueur joueur2 = p.tabJoueurs.get(Integer.parseInt(p.reponse));

		p.reponse = "";
		while (!(p.reponse.equalsIgnoreCase("Droite")) && !(p.reponse.equalsIgnoreCase("Gauche"))) {
			System.out.println("Choisissez la carte que vous voulez récupérer (Droite ou Gauche)");
			recupererInput();
		}
		String cote2 = p.reponse;

		p.tabJoueurs.get(p.joueurEnCours).changeProp(p.tabJoueurs.get(p.joueurEnCours), joueur2, cote1, cote2);

		String carteRecup = "";
		this.sendMessage("UpdateCartes");
		if (cote1.equalsIgnoreCase("Droite")) {
			System.out.println("Vous avez récupéré : " + p.tabJoueurs.get(p.joueurEnCours).carteDeux.getProp());
			carteRecup = p.tabJoueurs.get(p.joueurEnCours).carteDeux.getProp();
		} else {
			System.out.println("Vous avez récupéré : " + p.tabJoueurs.get(p.joueurEnCours).carteUne.getProp());
			carteRecup = p.tabJoueurs.get(p.joueurEnCours).carteUne.getProp();
		}

		// On demande si le joueur veut tenter le Trick
		this.sendMessage("DemanderTenterTrick-" + carteRecup);
		p.reponse = "";
		while (!(p.reponse.equalsIgnoreCase("Oui")) && !(p.reponse.equalsIgnoreCase("Non"))) {
			System.out.println("Voulez vous tenter le Trick ? (Oui ou Non)");
			recupererInput();
		}

		if (p.reponse.equalsIgnoreCase("Oui")) {
			// On teste si le Trick est validé ou non
			if (p.tabJoueurs.get(p.joueurEnCours).tenterTrick(p.pile.getTrick())) {
				if (p.pile.getTrick().getName().equalsIgnoreCase("The Other Hat Trick")) {
					p.finPartie = true;
				}
				System.out.println("Ta-Daaaah ! Trick réussi ! ( + " + p.pile.getTrick().getValue() + " points)");
				this.tourVariante -= 1;
				System.out.println("Les cartes utilisées sont :");
				System.out.println(p.tabJoueurs.get(p.joueurEnCours).carteUne.getProp() + " et "
						+ p.tabJoueurs.get(p.joueurEnCours).carteDeux.getProp());
				if (p.getVariante1() == false && p.getVariante2() == false) {
					p.tabJoueurs.get(p.joueurEnCours).addPoints(p.pile.getTrick().getValue(), 0);
				} else if (p.getVariante1() == true && p.getVariante2() == false) {
					p.tabJoueurs.get(p.joueurEnCours).addPoints(p.pile.getTrick().getValue(), this.tourVariante);
				} else if (p.getVariante1() == false && p.getVariante2() == true) {
					p.tabJoueurs.get(p.joueurEnCours).addPoints(p.pile.getTrick().getValue(), p.getNombreDeTours());
				}
				this.sendMessage("SuccessTrick");
				p.pile.removeLastTrick();

				if (p.finPartie == false) {
					// On demande quelle carte le joueur veut échanger avec celle du milieu
					p.reponse = "";
					while (!(p.reponse.equalsIgnoreCase("Droite")) && !(p.reponse.equalsIgnoreCase("Gauche"))) {
						System.out.println("Quelle carte souhaitez vous mettre au milieu ? (Droite ou Gauche)");
						recupererInput();
					}

					// On échange la carte et on les cache toutes
					p.changerCarteMilieu(p.tabJoueurs.get(p.joueurEnCours), p.reponse);
					p.carteMilieu.setVisible(false);
					p.tabJoueurs.get(p.joueurEnCours).carteUne.setVisible(false);
					p.tabJoueurs.get(p.joueurEnCours).carteDeux.setVisible(false);
					this.sendMessage("UpdateCartes");
				}
			} else {
				if (p.pile.getTrick().getName().equalsIgnoreCase("The Other Hat Trick")) {
					p.theOtherHatTrickCount++;
				}
				System.out.println("Dommage, le Trick est raté. Une prochaine fois !");
				this.sendMessage("FailTrick");
				p.tabJoueurs.get(p.joueurEnCours).penaltyTrick(p.tabJoueurs.get(p.joueurEnCours), p);
			}
		} else {
			if (p.pile.getTrick().getName().equalsIgnoreCase("The Other Hat Trick")) {
				p.theOtherHatTrickCount++;
			}
			System.out.println("Tant pis !");
			this.sendMessage("FailTrick");
			p.tabJoueurs.get(p.joueurEnCours).penaltyTrick(p.tabJoueurs.get(p.joueurEnCours), p);
		}
	}

	/**
	 * Demande au Joueur quelle carte il veut retourner si ses deux cartes sont
	 * cachées. Sinon, change les états automatiquement.
	 * 
	 * @param j
	 *            Joueur qui doit retourner une carte
	 */
	public void penaltyTrick(Joueur j, Partie p) {
		if (!j.carteUne.isVisible() && !j.carteDeux.isVisible()) {
			this.sendMessage("PenaltyTrick");
			p.reponse = "";
			while (!(p.reponse.equalsIgnoreCase("Droite")) && !(p.reponse.equalsIgnoreCase("Gauche"))) {
				System.out.println("Vous devez choisir une carte à retourner ! (Droite ou Gauche)");
				recupererInput();
			}
			if (p.reponse.equalsIgnoreCase("Droite")) {
				j.carteDeux.setVisible(true);
				System.out.println("Votre carte Droite est désormais visible ! (" + j.carteDeux.getProp() + ")");
			} else {
				j.carteUne.setVisible(true);
				System.out.println("Votre carte Gauche est désormais visible ! (" + j.carteUne.getProp() + ")");
			}
		} else if (j.carteUne.isVisible() && !j.carteDeux.isVisible()) {
			j.carteDeux.setVisible(true);
			System.out.println("Votre carte Droite est désormais visible ! (" + j.carteDeux.getProp() + ")");
		} else if (!j.carteUne.isVisible() && j.carteDeux.isVisible()) {
			j.carteUne.setVisible(true);
			System.out.println("Votre carte Gauche est désormais visible ! (" + j.carteUne.getProp() + ")");
		} else {
			System.out.println("Vos deux cartes sont déjà visibles, aucune pénalité ne vous est infligée.");
		}
	}
}

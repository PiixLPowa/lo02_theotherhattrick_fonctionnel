package fr.lo02.bessonpeuziat.vue;

import java.awt.Font;
import java.util.Observable;
import java.util.Observer;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import fr.lo02.bessonpeuziat.controleur.ControleurOuiNon;
import fr.lo02.bessonpeuziat.modele.Partie;

/**
 * InterfaceGameStart<br>
 * C'est la vue graphique qui sert à initialiser la partie.
 */

public class InterfaceGameStart implements Observer {

	/**
	 * Fenêtre principale de l'InterfaceGameStart
	 */
	private JFrame frame;

	/**
	 * Bouton "Oui"
	 */
	private JButton btnOui;

	/**
	 * Bouton "Non"
	 */
	private JButton btnNon;

	/**
	 * Bouton "1"
	 */
	private JButton btn1;

	/**
	 * Bouton "2"
	 */
	private JButton btn2;

	/**
	 * Texte à affiche pour poser les questions
	 */
	private JLabel txt;

	/**
	 * Zone de texte pour afficher les règles du jeu
	 */
	private JTextArea txtArea;

	/**
	 * Champ de texte pour entrer les pseudos des joueurs à la création de la partie
	 */
	private JTextField textField;

	/**
	 * Partie associée à l'interface
	 */
	protected Partie partie;

	/**
	 * Crée une instance de InterfaceGameStart
	 * 
	 * @param partie
	 *            Partie en cours
	 */
	public InterfaceGameStart(Partie partie) {
		initialize();
		this.partie = partie;
		this.partie.addObserver(this);
		new ControleurOuiNon(this.partie, this.btnOui, this.btnNon, this.btn1, this.btn2, this.textField);

		this.txtArea = new JTextArea();
		txtArea.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
		this.txtArea.setBounds(9, 82, 831, 339);
		this.txtArea.setText("The Other Hat Trick est un jeu de carte. "
				+ "\nL'objectif est de former des tricks à l'aide de cartes dites Props."
				+ "\nChaque joueur se voit distribuer deux props."
				+ "\nA son tour, celui-ci doit choisir entre un trick face visible et un trick face cachée à réaliser. "
				+ "\nUne fois le trick choisi, le joueur DOIT OBLIGATOIREMENT échanger un de ses props avec un prop d'un autre joueur face cachée ou face visible devant eux."
				+ "\nCela fait, le joueur tente alors le trick, si celui-ci est concluant il remporte le nombre de points indiqués. "
				+ "\nIl devra alors échanger un de ses props avec la carte du milieu, puis reposer se deux props face cachée devant lui. "
				+ "\nCependant si le trick n'est pas réalisé, il devra alors retourner face visible un de ses props devant lui. "
				+ "\nLors du dernier tour de jeu, un trick spécial apparait The Other Hat Trick. "
				+ "\nSon fonctionnement est un peu différent des autres, car si quelqu'un arrive à le réaliser, il gagne 6 points. "
				+ "\nNéanmoins, si personne ne le réussit, les joueurs possédant les cartes The Hat et The Other Rabbit ont une pénalité de 3 points. "
				+ "\nLe vainqueur est le joueur ayant accumulé le plus de points après ce tour. " + "\n\nVariante 1 : "
				+ "\nA chaque trick réussi, vous cumulez un malus s'appliquant au nombre de points que vous gagnerez sur le prochain trick réussi ! "
				+ "\n\nVariante 2 : "
				+ "\nIci, chaque changement de joueur augmente le nombre de points qu'un trick rapporte ! Gare aux fins de parties !"
				+ "\n\nExtension : " + "\nThe Carnivorous Rabbit et The Cap à la place de 2 Carrots. "
				+ "\nTrois nouveaux tricks sont réalisables : The Dead Of Rabbit, The Thug Rabbit et The Head Gear. "
				+ "\nNéanmoins, plus assez de carottes donc plus de The Bunch of Carrots !");
		frame.getContentPane().add(this.txtArea);
		this.txtArea.setVisible(false);
	}

	/**
	 * Initialise le contenu de la fenêtre
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setSize(850, 130);
		frame.setTitle("The Other Hat Trick");
		frame.setIconImage(new ImageIcon(getClass().getResource("/icone.png")).getImage());
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.getContentPane().setLayout(null);

		this.btnOui = new JButton("Oui");
		this.btnOui.setBounds(205, 41, 117, 29);
		frame.getContentPane().add(this.btnOui);

		this.btnNon = new JButton("Non");
		this.btnNon.setBounds(527, 41, 117, 29);
		frame.getContentPane().add(this.btnNon);

		this.btn1 = new JButton("1");
		this.btn1.setBounds(205, 41, 117, 29);

		this.btn2 = new JButton("2");
		this.btn2.setBounds(527, 41, 117, 29);

		this.txt = new JLabel();
		this.txt.setText("Voulez-vous consulter les règles du jeu ?");
		this.txt.setBounds(13, 17, 824, 16);
		frame.getContentPane().add(txt);

		this.textField = new JTextField();
		this.textField.setBounds(357, 41, 130, 26);
		this.textField.setColumns(10);
		this.textField.setVisible(false);
	}

	/**
	 * Facilite le changement du label txt<br>
	 * Cette méthode s'assure que le message affiché sera la bon<br>
	 * Vient en réponse à un problème rencontré de message qui ne s'actualisait pas
	 * 
	 * @param msg
	 *            Message à afficher sur la ligne de texte
	 */
	public void setMessageTxt(String msg) {
		this.txt.setVisible(false);
		frame.getContentPane().remove(this.txt);
		this.txt.setText(msg);
		frame.getContentPane().add(txt);
		this.txt.setVisible(true);
		frame.revalidate();
	}

	/**
	 * Met à jour la vue en fonction de l'argument envoyé
	 * 
	 * @param o
	 *            Observable qui a envoyé la notification
	 * 
	 * @param arg
	 *            Objet envoyé par l'Observable
	 */
	@Override
	public synchronized void update(Observable o, Object arg) {
		if (o instanceof Partie) {
			if (((String) arg).equals("DemandeRègles")) {
				this.txtArea.setVisible(true);
				frame.setSize(850, 495);
			}
			if (((String) arg).equals("AfficheQ2")) {
				setMessageTxt("Voulez-vous utiliser les règles standard ?");
			}
			if (((String) arg).equals("AfficheQ2")) {
				setMessageTxt("Voulez-vous utiliser les règles standard ?");
			}
			if (((String) arg).equals("AfficheChoixVariante")) {
				this.btnOui.setVisible(false);
				this.btnNon.setVisible(false);
				frame.getContentPane().remove(this.btnOui);
				frame.getContentPane().remove(this.btnNon);
				frame.getContentPane().add(this.btn1);
				frame.getContentPane().add(this.btn2);
				setMessageTxt("Quelle variante souhaitez vous utiliser ?");
			}
			if (((String) arg).equals("AfficheQExtension")) {
				this.btn1.setVisible(false);
				this.btn2.setVisible(false);
				frame.getContentPane().remove(this.btn1);
				frame.getContentPane().remove(this.btn2);
				frame.getContentPane().add(this.btnOui);
				frame.getContentPane().add(this.btnNon);
				this.btnOui.setVisible(true);
				this.btnNon.setVisible(true);
				setMessageTxt("Voulez-vous utiliser les extensions de carte ?");
			}
			if (((String) arg).equals("AfficheChoixJoueurs")) {
				this.txtArea.setVisible(false);
				frame.getContentPane().remove(this.txtArea);
				frame.setSize(850, 130);
				this.btnOui.setVisible(false);
				this.btnNon.setVisible(false);
				frame.getContentPane().remove(this.btnOui);
				frame.getContentPane().remove(this.btnNon);
				String msg = "Saisissez le nom des 3 joueurs (Tapez \"Virtuel\" pour ajouter un joueur virtuel)";
				setMessageTxt(msg);
				frame.getContentPane().add(this.textField);
				this.textField.setVisible(true);
			}
			if (((String) arg).equals("UpdateJoueurs")) {
				String msg = "Saisissez le nom des 3 joueurs (Tapez \"Virtuel\" pour ajouter un joueur virtuel)  : ";
				for (Integer j = 0; j < this.partie.getJoueurs().size(); j++) {
					if (j == 0) {
						msg = msg.concat(this.partie.getJoueurs().get(j).getName());
					} else {
						msg = msg.concat(" / " + this.partie.getJoueurs().get(j).getName());
					}
				}
				setMessageTxt(msg);
			}
			if (((String) arg).equals("FinInit")) {
				this.textField.setVisible(false);
				frame.getContentPane().remove(this.textField);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				frame.setVisible(false);
			}
		}

	}
}

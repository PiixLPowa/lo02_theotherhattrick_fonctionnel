package fr.lo02.bessonpeuziat.controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;

import javax.swing.JButton;
import javax.swing.JTextField;

import fr.lo02.bessonpeuziat.modele.Partie;

/**
 * ControleurOuiNon<br>
 * Controleur responsable de toutes les actions réalisées dans la Vue
 * InterfaceGameStart du début de partie.
 */

public class ControleurOuiNon extends Observable {

	/**
	 * Crée une instance de ControleurPlateau
	 * 
	 * @param p
	 *            Partie en cours
	 * @param btnOui
	 *            Bouton "Oui"
	 * @param btnNon
	 *            Bouton "Non"
	 * @param btn1
	 *            Bouton "1"
	 * @param btn2
	 *            Bouton "2"
	 * @param textField
	 *            Champ de texte
	 */
	public ControleurOuiNon(Partie p, JButton btnOui, JButton btnNon, JButton btn1, JButton btn2,
			JTextField textField) {
		this.addObserver(p);
		btnOui.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setChanged();
				notifyObservers("Oui");
			}
		});
		btnNon.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setChanged();
				notifyObservers("Non");
			}
		});
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setChanged();
				notifyObservers("1");
			}
		});
		btn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setChanged();
				notifyObservers("2");
			}
		});
		textField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setChanged();
				notifyObservers(textField.getText());
				textField.setText("");
				;
			}
		});
	}
}
